package com.liftekak.liftekak.models.distancematrix

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Sachinda on 2/14/2018.
 */
class DistanceMatrix {

    @SerializedName("destination_addresses")
    @Expose
    private var destinationAddresses: List<String>? = null
    @SerializedName("origin_addresses")
    @Expose
    private var originAddresses: List<String>? = null
    @SerializedName("rows")
    @Expose
    private var rows: List<Row>? = null
    @SerializedName("status")
    @Expose
    private var status: String? = null

    fun getDestinationAddresses(): List<String>? {
        return destinationAddresses
    }

    fun setDestinationAddresses(destinationAddresses: List<String>) {
        this.destinationAddresses = destinationAddresses
    }

    fun getOriginAddresses(): List<String>? {
        return originAddresses
    }

    fun setOriginAddresses(originAddresses: List<String>) {
        this.originAddresses = originAddresses
    }

    fun getRows(): List<Row>? {
        return rows
    }

    fun setRows(rows: List<Row>) {
        this.rows = rows
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }
}