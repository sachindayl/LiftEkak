package com.liftekak.liftekak.models.dbModels

/**
 * Created by Sachinda on 2/11/2018.
 */
data class UserVehicles(
        var make:String,
        var model:String,
        var year: String,
        var color: String,
        var plate: String,
        var created_at: String
){
    constructor(): this("","","","","","")
}