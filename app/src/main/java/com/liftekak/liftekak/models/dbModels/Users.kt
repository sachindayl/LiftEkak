package com.liftekak.liftekak.models.dbModels

import com.google.gson.annotations.SerializedName

/**
 * Created by Sachinda on 12/10/2017.
 * Users to
 */
data class Users(
        @SerializedName("first_name")
        var firstName: String,
        @SerializedName("last_name")
        var lastName: String,
        var email: String,
        @SerializedName("phone_number")
        var phoneNumber: String?,
        var password: String?,
        @SerializedName("is_admin")
        var isAdmin: String?,
        @SerializedName("created_at")
        var createdAt: Long?
) {
    constructor() : this("", "", "", null, null, null, null)

}

