package com.liftekak.liftekak.models.dbModels

import com.google.gson.annotations.SerializedName


data class ResponseModel(
        @SerializedName("success")
        var status: String?,
        var message: String?
) {
    constructor() : this(null, null)

    fun handleError() {

    }
}
