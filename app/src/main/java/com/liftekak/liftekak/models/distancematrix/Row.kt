package com.liftekak.liftekak.models.distancematrix

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Sachinda on 2/15/2018.
 */
class Row {

    @SerializedName("elements")
    @Expose
    private var elements: List<Element>? = null

    fun getElements(): List<Element>? {
        return elements
    }

    fun setElements(elements: List<Element>) {
        this.elements = elements
    }
}