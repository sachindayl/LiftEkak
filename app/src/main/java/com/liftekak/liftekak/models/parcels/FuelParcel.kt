package com.liftekak.liftekak.models.parcels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Sachinda on 4/2/2018.
 */
@Parcelize
data class FuelParcel(val diesel: String, val petrol: String, val petrolCity:String, val petrolOutStation: String):Parcelable