package com.liftekak.liftekak.models.dbModels

import com.google.gson.annotations.SerializedName

/**
 * Created by Sachinda on 12/10/2017.
 * Users to
 */
data class FuelPrice(
        @SerializedName("ID")
        var Id: Int,
        @SerializedName("fuel_type")
        var fuelType: String,
        @SerializedName("fuel_price")
        var fuelPrice: String,
        @SerializedName("city_mileage")
        var cityMileage: String,
        @SerializedName("out_station_mileage")
        var outStationMileage: String
) {
    constructor() : this(0,"", "", "","")
}

