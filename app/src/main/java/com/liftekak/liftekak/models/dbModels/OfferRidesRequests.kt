package com.liftekak.liftekak.models.dbModels

import com.google.gson.annotations.SerializedName
import java.net.CacheRequest

/**
 * Created by Sachinda on 2/11/2018.
 *
 */
data class OfferRidesRequests(
        @SerializedName("offer_ride_id")
        var requestId: Int,
        @SerializedName("request_user_id")
        var requestUserId: Int,
        @SerializedName("request_approved")
        var isApproved: Boolean?,
        @SerializedName("request_rejected")
        var isReject: Boolean?
) {
    constructor() : this(0,0,null, null)
}