package com.liftekak.liftekak.models.parcels

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import kotlinx.android.parcel.Parcelize

/**
 * Created by Sachinda on 4/2/2018.
 */
@Parcelize
data class PolylineParcel(val polyLineSequenceList: ArrayList<ArrayList<LatLng>>, val polyLineBoundsList: ArrayList<LatLngBounds>):Parcelable