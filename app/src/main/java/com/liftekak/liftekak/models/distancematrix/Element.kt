package com.liftekak.liftekak.models.distancematrix

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Sachinda on 2/14/2018.
 */
class Element {
    @SerializedName("distance")
    @Expose
    private var distance: Distance? = null
    @SerializedName("duration")
    @Expose
    private var duration: Duration? = null
    @SerializedName("status")
    @Expose
    private var status: String? = null

    fun getDistance(): Distance? {
        return distance
    }

    fun setDistance(distance: Distance) {
        this.distance = distance
    }

    fun getDuration(): Duration? {
        return duration
    }

    fun setDuration(duration: Duration) {
        this.duration = duration
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }
}