package com.liftekak.liftekak.models

/**
 * Created by Sachinda on 2/3/2018.
 */
data class CipherEncrypt(
        var password: String,
        var pLength: Int
){
    constructor():this("",0)

}