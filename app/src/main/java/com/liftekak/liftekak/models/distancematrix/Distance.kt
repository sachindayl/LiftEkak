package com.liftekak.liftekak.models.distancematrix

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Sachinda on 2/14/2018.
 */
class Distance{

    @SerializedName("text")
    @Expose
    private var text: String? = null
    @SerializedName("value")
    @Expose
    private var value: Int? = null

    fun getText(): String? {
        return text
    }

    fun setText(text: String) {
        this.text = text
    }

    fun getValue(): Int? {
        return value
    }

    fun setValue(value: Int?) {
        this.value = value
    }
}