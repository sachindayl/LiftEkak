package com.liftekak.liftekak.models.dbModels

import com.google.gson.annotations.SerializedName
import java.net.CacheRequest

/**
 * Created by Sachinda on 2/11/2018.
 *
 */
data class OfferRides(
        @SerializedName("request_id")
        var requestId: Int,
        @SerializedName("from_name")
        var fromName: String,
        @SerializedName("from_address")
        var fromAddress: String,
        @SerializedName("from_latitude")
        var fromLatitude: Double,
        @SerializedName("from_longitude")
        var fromLongitude: Double,
        @SerializedName("to_name")
        var toName: String,
        @SerializedName("to_address")
        var toAddress: String,
        @SerializedName("to_latitude")
        var toLatitude: Double,
        @SerializedName("to_longitude")
        var toLongitude: Double,
        @SerializedName("phone_number")
        var phoneNumber: String,
        var date: String,
        var time: String,
        var distance: String,
        var duration: String,
        var recurrence: String,
        @SerializedName("passenger_type")
        var passengerType: String,
        @SerializedName("number_of_passengers")
        var numberOfPassengers: Int?,
        var polyline: String,
        @SerializedName("user_id")
        var userId: String,
        @SerializedName("created_at")
        var createdAt: String,
        @SerializedName("image")
        var image: String?
) {
    constructor() : this(0,"", "", 0.0, 0.0, "", "", 0.0, 0.0, "", "", "", "", "", "", "", null, "", "", "", null)
}