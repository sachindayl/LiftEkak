package com.liftekak.liftekak.ui.myprofile


import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.constants.MapConstants
import com.liftekak.liftekak.base.constants.OtherConstants
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.Users
import com.liftekak.liftekak.ui.tabhost.TabHostFragment
import kotlinx.android.synthetic.main.fragment_drive_details.*
import kotlinx.android.synthetic.main.fragment_my_profile.*
import timber.log.Timber
import java.io.File
import java.io.IOException


/**
 * A simple [Fragment] subclass.
 */
class MyProfileFragment : BaseTabFragment(), MyProfileContract.View {

    private lateinit var presenter: MyProfileContract.Presenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        presenter = MyProfilePresenterImpl(this, mContext, mDatabase, mAuth, dbUrl)
        val bundle = Bundle()
        bundle.putInt("isProfileFrag", 1)

        val tabHostFragment = TabHostFragment()
        tabHostFragment.arguments = bundle
        this.childFragmentManager.beginTransaction()
                .replace(R.id.fl_mp_rides_container, tabHostFragment)
                .commit()

        iv_mp_upload_icon.setOnClickListener {
            showToast("upload icon clicked.")
            openGallery()
        }
    }

    private fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.action = Intent.ACTION_OPEN_DOCUMENT
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
        } else {
            intent.action = Intent.ACTION_GET_CONTENT
        }

        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), OtherConstants.PICK_IMAGE_REQUEST)

    }

    override fun showUserInfo(user: Users) {
        val name = "${user.firstName} ${user.lastName}"
        tv_mp_user_name.text = name
    }

    /**
     * User image upload is done here
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.v("Req code, Result code %s, %s", requestCode, resultCode)
        if (resultCode != Activity.RESULT_OK) return
        if (data == null) return
        if (requestCode == OtherConstants.PICK_IMAGE_REQUEST) {

            val originalUri = data.data
            val takeFlags = data.flags and (Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            val checkCallPermission = ContextCompat.checkSelfPermission(tabAct, android.Manifest.permission.READ_EXTERNAL_STORAGE)

            if (checkCallPermission == PackageManager.PERMISSION_GRANTED) {
                try {
                    mContext.contentResolver.takePersistableUriPermission(originalUri, takeFlags)
                    val id = originalUri.lastPathSegment.split(":")[1]
                    Timber.v("image id %s", id)
                    val imageColumns = arrayOf(MediaStore.Images.Media.DATA)
                    val state = Environment.getExternalStorageState()
                    val uri = if (!state.equals(Environment.MEDIA_MOUNTED, true))
                        MediaStore.Images.Media.INTERNAL_CONTENT_URI
                    else MediaStore.Images.Media.EXTERNAL_CONTENT_URI


                    val bitmap = MediaStore.Images.Media.getBitmap(mContext.contentResolver, originalUri)
                    val roundDrawable = RoundedBitmapDrawableFactory.create(resources, bitmap)
                    roundDrawable.isCircular = true
                    iv_mp_user_image.setImageDrawable(roundDrawable)

                    var selectedImagePath = "path"

                    val imageCursor = mContext.contentResolver.query(uri, imageColumns,
                            MediaStore.Images.Media._ID + "=" + id, null, null)
                    if (imageCursor.moveToFirst())
                        selectedImagePath = imageCursor
                                .getString(imageCursor.getColumnIndex(MediaStore.Images.Media.DATA))



                    imageCursor.close()
                    Timber.v("image value %s", selectedImagePath)
                    val file = File(selectedImagePath)
                    presenter.uploadImage(file)
                } catch (e: IOException) {
                    e.printStackTrace()
                    showSnackBar(cl_dd, "Permission to call has been denied by the user.")
                }

            } else {
                ActivityCompat.requestPermissions(tabAct, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE), MapConstants.PERMISSIONS_REQUEST_READ_EXTERNAL_FILES)
            }

        }
    }

    override fun setProfileImage() {
        val userId = Prefs(mContext).getUserId()
        val userImage = Prefs(mContext).getUserImage()
        if (userImage != "") {
            Glide.with(tabAct)
                    .load("${dbUrl}images/$userId/$userImage")
                    .apply(RequestOptions.circleCropTransform())
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(iv_mp_user_image)
        } else {
            iv_mp_user_image.setImageResource(R.mipmap.ic_launcher_round)
        }
    }


    override fun onStart() {
        super.onStart()
        tabAct.supportActionBar?.title = "My Profile"
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.requestUserInfo()
        setProfileImage()
    }

    override fun showLoadingIndicator() {
        pb_mp?.visibility = View.VISIBLE
    }

    override fun hideLoadingIndicator() {
        pb_mp?.visibility = View.GONE
    }
}
