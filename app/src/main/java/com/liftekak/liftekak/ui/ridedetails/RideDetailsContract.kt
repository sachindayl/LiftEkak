package com.liftekak.liftekak.ui.ridedetails

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Polyline
import com.liftekak.liftekak.models.dbModels.Users

/**
 * Created by Sachinda on 4/2/2018.
 */
interface RideDetailsContract{
    interface View{
        fun showPath(polyLineSequenceList: ArrayList<ArrayList<LatLng>>, polyLineBoundsList: ArrayList<LatLngBounds>)
        fun stylePolylines(polyline: Polyline)
        fun showRiderDetails(user: Users)
        fun buttonSetup()
        fun showMyProfileButtons()
        fun showRideProfileButtons()
        fun showToast(message: String)
        fun gotoTabFrag()
    }
    interface Presenter{
        fun decodePolyline(polyline: String)
        fun riderDetails(userId: String, isMyProfile: Int)
        fun deleteRequest(requestId: Int)
    }
}