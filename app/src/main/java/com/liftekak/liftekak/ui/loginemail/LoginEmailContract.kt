package com.liftekak.liftekak.ui.loginemail

import com.google.firebase.auth.FirebaseUser
import com.liftekak.liftekak.models.dbModels.Users

/**
 * Created by Sachinda on 2/3/2018.
 */
interface LoginEmailContract{
    interface View{
        fun goToSignIn()
        fun checkForErrors(): Boolean
        fun setupButtons()
        fun checkRegEx()
        fun gotoUserDashboard(user : FirebaseUser)
        fun showToast(message:String)
    }

    interface Presenter{

        fun isPasswordMatching(password: String, confirmation: String): Boolean
        fun createUser(newUser: Users)
        fun registerUserOnDB(newUser: Users)
    }
}