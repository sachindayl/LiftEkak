package com.liftekak.liftekak.ui.offerride

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.liftekak.liftekak.base.GMapSupport
import com.liftekak.liftekak.services.RetrofitService
import com.liftekak.liftekak.models.dbModels.OfferRides
import com.liftekak.liftekak.models.directions.Directions
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


/**
 * Created by Sachinda on 2/10/2018.
 *
 */
class OfferRidePresenterImpl(var view: OfferRideContract.View?, val mContext: Context,
                             val mDatabase: DatabaseReference, val mAuth: FirebaseAuth,
                             private val dbUrl: String) : OfferRideContract.Presenter {

    var routesList = ArrayList<String>()
    var distancesList = ArrayList<String>()
    var durationsList = ArrayList<String>()
    private var insertDriveCall: Disposable? = null
    private var distanceCall: Disposable? = null
    /**
     * Create Ride Offer request in the DB
     */
    override fun createDriveRequest(offerRide: OfferRides) {
        offerRide.numberOfPassengers = 0
        val numOfPassengers = offerRide.numberOfPassengers as Int
        val userId = mAuth.currentUser?.uid as String
        offerRide.userId = userId
        val offerRideTemp = replaceCharsForStorage(offerRide)

        val retrofit = RetrofitService().offerRidesAPIService(dbUrl)
        insertDriveCall = retrofit.insertDrive(
                offerRideTemp.fromName, offerRideTemp.fromAddress, offerRideTemp.fromLatitude,
                offerRideTemp.fromLongitude, offerRideTemp.toName, offerRideTemp.toAddress,
                offerRideTemp.toLatitude, offerRideTemp.toLongitude, offerRideTemp.phoneNumber,
                offerRideTemp.date, offerRideTemp.time, offerRideTemp.distance, offerRideTemp.duration,
                offerRideTemp.recurrence, offerRideTemp.passengerType, numOfPassengers, offerRideTemp.polyline,
                offerRideTemp.userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete { view?.goToTabFrag() }
                .subscribe({ s -> view?.showToast(s.message as String) }, { e ->
                    view?.showToast("Request could not be created. Please try again later.")
                    Timber.v(e, "insertDrive error: %s", e.localizedMessage)
                })

    }

    /**
     * Checks if from and to boxes are empty
     */
    override fun checkForDistanceValues(from: String, to: String): Boolean {
        var distanceAvailable = false
        if (from != "" && to != "") distanceAvailable = true
        return distanceAvailable
    }

    /**
     * Calls Directions API to find routes
     */
    override fun findRoutes(fromLatLng: LatLng, toLatLng: LatLng, key: String) {

        val directionsApi = RetrofitService().mapService()
        val units = "metric"
        val origin = "${fromLatLng.latitude},${fromLatLng.longitude}"
        val destination = "${toLatLng.latitude},${toLatLng.longitude}"
        try {
            distanceCall = directionsApi.getDirections(units, origin, destination, true, key)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { view?.showLoadingIndicator() }
                    .doOnComplete { view?.hideLoadingIndicator() }
                    .subscribe(
                            { s -> routesOrganizer(s) },
                            { e ->
                                view?.showToast("Routes could not be found. Please try again later.")
                                Timber.v(e, "Get Drives error: %s", e.localizedMessage)
                            })

        } catch (e: Exception) {
            Timber.v("Error: %s", e.message)
        }

    }

    private fun routesOrganizer(directionsResponse: Directions) {
        val gMapSupport = GMapSupport()
        var bounds = ArrayList<LatLngBounds>()
        var path = ArrayList<ArrayList<LatLng>>()
        routesList.clear()
        val directions: Directions = directionsResponse
        val numOfRoutes = directions.routes?.size
        for (index in 0 until numOfRoutes!!) {
            val distance = directions.routes?.get(index)?.legs?.get(0)?.distance?.text as String
            val duration = directions.routes?.get(index)?.legs?.get(0)?.duration?.text as String
            val polyLine = directions.routes?.get(index)?.overviewPolyline?.points as String
            distancesList.add(distance)
            durationsList.add(duration)
            routesList.add(polyLine)
        }
        Single.fromCallable {
            path = gMapSupport.decodePolyLine(routesList)
            bounds = gMapSupport.createDirectionBounds(path)
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { view?.showPolyline(path, bounds) }
                .subscribe()
    }

    private fun replaceCharsForStorage(request: OfferRides): OfferRides {
        val fromName = request.fromName.replace('\\', '%').replace('\'', '*')
        val fromAddress = request.fromAddress.replace('\\', '%').replace('\'', '*')
        val toName = request.toName.replace('\\', '%').replace('\'', '*')
        val toAddress = request.toAddress.replace('\\', '%').replace('\'', '*')
        val polyline = request.polyline.replace('\\', '%').replace('\'', '*')
        request.fromName = fromName
        request.fromAddress = fromAddress
        request.toName = toName
        request.toAddress = toAddress
        request.polyline = polyline
        return request
    }

    override fun pickDistance(index: String): String {
        val i = index.toInt()
        return distancesList[i]
    }

    override fun pickDuration(index: String): String {
        val i = index.toInt()
        return durationsList[i]
    }

    override fun pickRoute(index: String): String {
        val i = index.toInt()
        return routesList[i]
    }

    override fun onDestroy() {
        view = null
        if (insertDriveCall?.isDisposed == false) insertDriveCall?.dispose()
        if (distanceCall?.isDisposed == false) distanceCall?.dispose()
    }
}