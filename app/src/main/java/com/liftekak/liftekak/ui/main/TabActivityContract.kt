package com.liftekak.liftekak.ui.main

/**
 * Created by Sachinda on 4/6/2018.
 */
interface TabActivityContract{
    interface Presenter{
        fun pullFuelPrice()
        fun isUserIdAvailable():Boolean
        fun getUserId()
    }
}