package com.liftekak.liftekak.ui.loginselection


import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FirebaseUser
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseFragment
import com.liftekak.liftekak.base.constants.MapConstants
import com.liftekak.liftekak.ui.loginemail.LoginEmailFragment
import com.liftekak.liftekak.ui.main.TabActivity
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_login_selection.*
import org.json.JSONException
import timber.log.Timber


/**
 * A simple [Fragment] subclass.
 */
class LoginSelectionFragment : BaseFragment(), LoginSelectionContract.View {

    private lateinit var presenter: LoginSelectionContract.Presenter
    private lateinit var mCallbackManager: CallbackManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainAct.appbar_layout.visibility = View.GONE
        presenter = LoginSelectionPresenterImpl(this, mAuth, mDatabase)
        mCallbackManager = CallbackManager.Factory.create()
        tv_register.setOnClickListener {
            Timber.v("Register Frag setOnClickListener")
            goToRegister()
        }


        LoginManager.getInstance().registerCallback(mCallbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        Timber.d("Login Success!")

                    }

                    override fun onCancel() {
                        Toast.makeText(mainAct, "Login Cancelled", Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(exception: FacebookException) {
                        Toast.makeText(mainAct, exception.message, Toast.LENGTH_LONG).show()
                    }
                })

        btn_login.setOnClickListener {
            val email = et_email.text.toString()
            val pass = et_password.text.toString()
//            val encryptPass = Encrypt(mContext)
//            val encryptedPass = encryptPass.encrypt(pass)
            presenter.validateUserByEmail(email, pass)
        }
        btn_fb_login.text = resources.getString(R.string.sign_in_with_facebook)
        btn_fb_login.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, arrayListOf("email", "public_profile"))
            fbRegisterCallback()
        }

    }

    override fun goToRegister() {
        val fm = mainAct.supportFragmentManager
        fm.beginTransaction()
                .replace(R.id.main_container, LoginEmailFragment())
                .addToBackStack("Register")
                .commit()

        Timber.v("Visited Register Fragment")
    }

    override fun onStart() {
        super.onStart()
        val checkFinePermission = ContextCompat.checkSelfPermission(mainAct, android.Manifest.permission.ACCESS_FINE_LOCATION)
        val checkCoarsePermission = ContextCompat.checkSelfPermission(mainAct, android.Manifest.permission.ACCESS_COARSE_LOCATION)
        if (checkFinePermission == PackageManager.PERMISSION_GRANTED && checkCoarsePermission == PackageManager.PERMISSION_GRANTED) {
            val currentUser: FirebaseUser? = mAuth.currentUser
            if (currentUser != null) {
                goToUserDashboard()
            }
        } else {
            ActivityCompat.requestPermissions(mainAct, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION), MapConstants.PERMISSIONS_REQUEST_ACCESS_LOCATION)
        }
    }

    override fun goToUserDashboard() {
        Timber.v("Visiting Login Select go to user dashboard")
        startActivity(Intent(mainAct, TabActivity::class.java))
        mainAct.finish()
    }


    /**
     * Callback token for Facebook login users
     * Bundles Name and email
     */
    override fun fbRegisterCallback() {
        LoginManager.getInstance().registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                Timber.v("facebook:onSuccess: %s", result)
                val accessToken = result.accessToken
                Timber.v("Access Token: %s", accessToken)
                //TODO: Not working need fix
//                val profile = Profile.getCurrentProfile()
//                val fName = profile.firstName
//                val lName = profile.lastName
//                var picture = profile.getProfilePictureUri(50,50)
                var fName = ""
                var lName = ""
                var email: String? = ""

                GraphRequest.newMeRequest(accessToken) { jObject, _ ->
                    Timber.v("LoginActivity Response: %s", jObject.toString())

                    try {
                        fName = jObject.getString("first_name")
                        lName = jObject.getString("last_name")
                        email = jObject?.getString("email")
                        Timber.v("Email = %s", email)

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                Timber.v("FB Name %s", fName)
                val fbList = ArrayList<String>()
                fbList.add(fName)
                fbList.add(lName)
                fbList.add(email as String)
                val profileBundle = Bundle()
                profileBundle.putStringArrayList("fbProfile", fbList)
                presenter.validateUserByFacebook(result.accessToken, profileBundle)


            }

            override fun onCancel() {
            }

            override fun onError(error: FacebookException?) {
                showSnackBar(cl_ls, "Authentication Failed.")
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mCallbackManager.onActivityResult(requestCode, resultCode, data)
    }

//    override fun changeFbButtonText() {
//        btn_fb_custom.text = resources.getString(R.string.log_out)
//        btn_fb_login.unregisterCallback(mCallbackManager)
//    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
