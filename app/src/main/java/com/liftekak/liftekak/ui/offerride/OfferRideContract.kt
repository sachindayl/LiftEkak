package com.liftekak.liftekak.ui.offerride

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.liftekak.liftekak.models.dbModels.OfferRides

/**
 * Created by Sachinda on 2/10/2018.
 * For Drivers that will provide transportation. They can create a request to find passengers
 */
interface OfferRideContract{
    interface View{
        fun spinnerSetup()
        fun placesAutoCompleteSetup()
        fun showNumber()
        fun showToast(message:String)
        fun goToTabFrag()
        fun checkForEmptyFields():Boolean
        fun showLocationInfo(distanceValue: String?, durationValue: String?)
        fun showPolyline(polyLineSequenceList: ArrayList<ArrayList<LatLng>>, polyLineBoundsList: ArrayList<LatLngBounds>)
        fun stylePolylines()
        fun showLoadingIndicator()
        fun hideLoadingIndicator()
        fun dateTimePickerSetup()
    }

    interface Presenter{
        fun createDriveRequest(offerRide: OfferRides)
        fun checkForDistanceValues(from: String, to:String) : Boolean
        fun findRoutes(fromLatLng: LatLng, toLatLng: LatLng, key:String)
        fun pickRoute(index:String): String
        fun pickDistance(index:String): String
        fun pickDuration(index:String): String
        fun onDestroy()
    }
}