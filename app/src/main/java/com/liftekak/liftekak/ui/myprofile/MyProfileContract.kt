package com.liftekak.liftekak.ui.myprofile

import com.liftekak.liftekak.models.dbModels.Users
import java.io.File

/**
 * Created by Sachinda on 4/5/2018.
 */
interface MyProfileContract {
    interface View {
        fun showUserInfo(user: Users)
        fun showToast(message: String)
        fun showLoadingIndicator()
        fun hideLoadingIndicator()
        fun setProfileImage()
    }

    interface Presenter {
        fun requestUserInfo()
        fun uploadImage(file: File)

        fun onDestroy()
    }
}