package com.liftekak.liftekak.ui.main

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.facebook.AccessToken
import com.google.firebase.auth.FirebaseAuth
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.LiftEkakApplication
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.ui.findrides.FindRidesFragment
import com.liftekak.liftekak.ui.finddrives.FindDrivesFragment
import com.liftekak.liftekak.ui.loginselection.LoginSelectionFragment
import com.liftekak.liftekak.ui.vehicleselect.VehicleSelectFragment
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var mPrefs: Prefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar?>(R.id.toolbar)
        setSupportActionBar(toolbar)
        mPrefs = Prefs(LiftEkakApplication.applicationContext())
//        val toggle = ActionBarDrawerToggle(
//                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
//        drawer_layout.addDrawerListener(toggle)
//        toggle.syncState()

//        nav_view.setNavigationItemSelectedListener(this)
        if (findViewById<View>(R.id.main_container) != null) {
            if (savedInstanceState != null) {
                return
            }

            val loginSelectionFrag = LoginSelectionFragment()
//            loginSelectionFrag.arguments = intent.extras
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.main_container, loginSelectionFrag, "LoginSelection")
            fragmentTransaction.commit()
        }

    }

    override fun onBackPressed() {
        val fragCount = supportFragmentManager.backStackEntryCount
        Timber.v("FragmentCount: %s", fragCount)
        if (fragCount == 0) {
            finish()
        }
//        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
//            drawer_layout.closeDrawer(GravityCompat.START)
//        } else {
//            super.onBackPressed()
//        }
        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_find_rides -> {
                val findRidesFrag = FindDrivesFragment()
                supportFragmentManager.beginTransaction()
                        .replace(R.id.main_container, findRidesFrag)
                        .addToBackStack("Find Rides")
                        .commit()
            }

            R.id.nav_find_passengers -> {
                val findPassengersFrag = FindRidesFragment()
                supportFragmentManager.beginTransaction()
                        .replace(R.id.main_container, findPassengersFrag)
                        .addToBackStack("Offer Rides")
                        .commit()
            }

            R.id.nav_select_vehicle -> {
                val vehicleSelectFrag = VehicleSelectFragment()
                supportFragmentManager.beginTransaction()
                        .replace(R.id.main_container, vehicleSelectFrag)
                        .addToBackStack("Vehicle Select")
                        .commit()
            }

            R.id.nav_log_out -> {

                val fm = supportFragmentManager
                for (i in 0 until fm.backStackEntryCount) {
                    fm.popBackStack()
                }
                fm.beginTransaction()
                        .replace(R.id.main_container, LoginSelectionFragment())
                        .commit()
                FirebaseAuth.getInstance().signOut()
                if (AccessToken.getCurrentAccessToken() != null) {
                    AccessToken.setCurrentAccessToken(null)
                }

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
