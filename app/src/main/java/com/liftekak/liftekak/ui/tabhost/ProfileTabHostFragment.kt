package com.liftekak.liftekak.ui.tabhost


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTabHost
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.LiftEkakApplication
import com.liftekak.liftekak.ui.finddrives.FindDrivesFragment
import com.liftekak.liftekak.ui.findrides.FindRidesFragment
import kotlinx.android.synthetic.main.app_bar_tab.*


/**
 * A simple [Fragment] subclass.
 */
class ProfileTabHostFragment : BaseTabFragment() {


    private var mTabHost: FragmentTabHost? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_host, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct.appbar_tab_layout.visibility = View.VISIBLE
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        mTabHost = tabAct.findViewById(android.R.id.tabhost)
        mTabHost?.setup(tabAct, childFragmentManager, android.R.id.tabcontent)

        mTabHost!!.addTab(mTabHost!!.newTabSpec("Drives")!!.setIndicator("Drives"),
                FindDrivesFragment::class.java, null)
        mTabHost!!.addTab(mTabHost!!.newTabSpec("Rides")!!.setIndicator("Rides"),
                FindRidesFragment::class.java, null)
        mTabHost!!.tabWidget.setBackgroundColor(ContextCompat.getColor(LiftEkakApplication.applicationContext(), R.color.colorPrimary))
        (0 until mTabHost!!.tabWidget.childCount)
                .map { mTabHost!!.tabWidget.getChildAt(it)!!.findViewById(android.R.id.title) as TextView }
                .forEach { it.setTextColor(ContextCompat.getColor(LiftEkakApplication.applicationContext(), R.color.colorTextPrimary)) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mTabHost = null
    }
}// Required empty public constructor
