package com.liftekak.liftekak.ui.ridedetails


import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.constants.MapConstants
import com.liftekak.liftekak.models.dbModels.Users
import com.liftekak.liftekak.models.parcels.PolylineParcel
import com.liftekak.liftekak.ui.mapview.MapViewFragment
import kotlinx.android.synthetic.main.fragment_drive_details.*
import kotlinx.android.synthetic.main.fragment_ride_details.*

/**
 * A simple [Fragment] subclass.
 *
 */
class RideDetailsFragment : BaseTabFragment(), RideDetailsContract.View, OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private var rideDetailsBundle: Bundle? = null
    private lateinit var presenter: RideDetailsContract.Presenter
    private lateinit var gMap: GoogleMap
    private lateinit var detailsList: ArrayList<String>
    private lateinit var startMarkerOptions: MarkerOptions
    private lateinit var endMarkerOptions: MarkerOptions
    private lateinit var polylineOptions: PolylineOptions
    private lateinit var polyLineSequenceListCopy: ArrayList<ArrayList<LatLng>>
    private lateinit var polyLineBoundsListCopy: ArrayList<LatLngBounds>
    private var isProfileFrag: Int? = null
    private var buttonType: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_ride_details, container, false)
        rideDetailsBundle = arguments
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct.supportActionBar!!.title = "Ride Details"
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        detailsList = rideDetailsBundle?.getStringArrayList("rideDetails")!!
        presenter = RideDetailsPresenterImpl(this, mContext, mDatabase, mAuth, dbUrl)
        val mapFragment = this.childFragmentManager.findFragmentById(R.id.map_rd) as SupportMapFragment
        mapFragment.getMapAsync(this)
        isProfileFrag = detailsList[15].toInt()
        tv_rd_from_location.text = detailsList[1]
        tv_rd_to_location.text = detailsList[5]
        tv_rd_date.text = detailsList[9]
        tv_rd_time.text = detailsList[10]
        tv_rd_recurrence.text = detailsList[11]
        presenter.riderDetails(detailsList[14], isProfileFrag as Int)
    }

    /**
     * Button text and listeners are set according to buttonType. If 0, then its another user,
     * if 1, its user's own request
     */
    override fun buttonSetup() {
        if (buttonType == 0) {
            btn_rd_left.setOnClickListener {
                val number = detailsList[13]
                val intent = Intent(Intent.ACTION_CALL)
                intent.data = Uri.parse("tel:$number")
                if (number == "") {
                    showSnackBar(cl_dd, "Number is not available.")
                } else {
                    val checkCallPermission = ContextCompat.checkSelfPermission(tabAct, android.Manifest.permission.CALL_PHONE)
                    if (checkCallPermission == PackageManager.PERMISSION_GRANTED) {
                        try {
                            startActivity(intent)
                        } catch (e: SecurityException) {
                            e.printStackTrace()
                            showSnackBar(cl_dd, "Permission to call has been denied by the user.")
                        }

                    } else {
                        ActivityCompat.requestPermissions(tabAct, arrayOf(android.Manifest.permission.CALL_PHONE), MapConstants.PERMISSIONS_REQUEST_CALL_PHONE)
                    }
                }
            }
            btn_rd_right.setOnClickListener {
                showToast("Requesting Drive")
            }

        }else{
            btn_rd_left.setOnClickListener{
                showToast("Disable Button Pressed")
            }
            btn_rd_right.setOnClickListener {
                AlertDialog.Builder(tabAct)
                        .setTitle("Delete Ride")
                        .setMessage("Are you sure you want to delete this ride request?")
                        .setPositiveButton("Yes") { _, _ ->
                            presenter.deleteRequest(detailsList[0].toInt())
                        }
                        .setNegativeButton("No", null)
                        .show()

            }
        }

    }

    override fun showPath(polyLineSequenceList: ArrayList<ArrayList<LatLng>>, polyLineBoundsList: ArrayList<LatLngBounds>) {
        polyLineSequenceListCopy = polyLineSequenceList
        polyLineBoundsListCopy = polyLineBoundsList
        val polylineOpt = PolylineOptions()
        polyLineSequenceList[0].forEach { points -> polylineOpt.add(points) }
        gMap.clear()
        polylineOptions = polylineOpt
        val polyline = gMap.addPolyline(polylineOpt)
        val bounds: LatLngBounds = polyLineBoundsList[0]
        stylePolylines(polyline)
        startMarkerOptions = MarkerOptions().position(polyLineSequenceList[0][0])
        endMarkerOptions = MarkerOptions().position(polyLineSequenceList[0][polyLineSequenceList[0].lastIndex])
        gMap.addMarker(startMarkerOptions)
        gMap.addMarker(endMarkerOptions)
        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 10)
        gMap.animateCamera(cameraUpdate)
        gMap.uiSettings.isScrollGesturesEnabled = false
    }

    override fun showRiderDetails(user: Users) {
        val riderName = "${user.firstName} ${user.lastName}"
        tv_rd_rider_name.text = riderName
    }

    override fun showMyProfileButtons() {
        buttonType = 1
        btn_rd_left.text = resources.getString(R.string.requests)
        btn_rd_right.text = resources.getString(R.string.delete_request)
        buttonSetup()
    }

    override fun showRideProfileButtons() {
        buttonType = 0
        btn_rd_left.text = resources.getString(R.string.call_for_more_info)
        btn_rd_right.text = resources.getString(R.string.request_drive)
        buttonSetup()
    }

    override fun stylePolylines(polyline: Polyline) {
        polyline.width = 10f
        polyline.color = ContextCompat.getColor(mContext, R.color.purple300)
    }

    override fun onMapClick(p0: LatLng?) {
        val bundle = Bundle()
        bundle.putParcelable("mapParcel", PolylineParcel(polyLineSequenceListCopy, polyLineBoundsListCopy))
        val mapViewFragment = MapViewFragment()
        mapViewFragment.arguments = bundle
        tabAct.supportFragmentManager.beginTransaction()
                .replace(R.id.main_tab_container, mapViewFragment)
                .addToBackStack("Map View")
                .commit()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        gMap = googleMap as GoogleMap
        gMap.setMinZoomPreference(1.0f)
        gMap.setMaxZoomPreference(20.0f)
        val colombo = LatLng(6.927, 79.861)
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(colombo, 10f))
        presenter.decodePolyline(detailsList[12])
        gMap.setOnMapClickListener(this)
    }

    override fun gotoTabFrag() {
        val fm = tabAct.supportFragmentManager
        fm.popBackStack()
//        fm.beginTransaction()
//                .addToBackStack(null)
//                .replace(R.id.main_tab_container, TabHostFragment())
//                .commit()
    }

}
