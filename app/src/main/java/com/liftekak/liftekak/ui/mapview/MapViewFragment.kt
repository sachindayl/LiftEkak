package com.liftekak.liftekak.ui.mapview


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.models.parcels.PolylineParcel

/**
 * A simple [Fragment] subclass.
 *
 */
class MapViewFragment : BaseTabFragment(), OnMapReadyCallback {

    private lateinit var gMap: GoogleMap
//    private var polylineModel: PolylineModel? = null
    private lateinit var polylineParcel: PolylineParcel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct.supportActionBar!!.title = "Map Info"
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val bundle = this.arguments
        if (bundle != null) polylineParcel = bundle.getParcelable("mapParcel")
        val mapFragment = this.childFragmentManager.findFragmentById(R.id.map_mv) as SupportMapFragment
        mapFragment.getMapAsync(this)
//        polylineModel = LiftEkakApplication.polylineModel

    }

    private fun showPath() {
        val polylineOpt = PolylineOptions()
        polylineParcel.polyLineSequenceList[0].forEach { points -> polylineOpt.add(points) }
        gMap.clear()
        val polyline = gMap.addPolyline(polylineOpt)
        val bounds: LatLngBounds = polylineParcel.polyLineBoundsList[0]
        stylePolylines(polyline)
        gMap.addMarker(MarkerOptions().position(polylineParcel.polyLineSequenceList[0][0]))
        gMap.addMarker(MarkerOptions().position(polylineParcel.polyLineSequenceList[0][polylineParcel.polyLineSequenceList[0].lastIndex]))
        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 10)
        gMap.animateCamera(cameraUpdate)
    }

    private fun stylePolylines(polyline: Polyline) {
        polyline.width = 10f
        polyline.color = ContextCompat.getColor(mContext, R.color.purple300)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        gMap = googleMap as GoogleMap
        gMap.setMinZoomPreference(1.0f)
        gMap.setMaxZoomPreference(20.0f)
        val colombo = LatLng(6.927, 79.861)
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(colombo, 10f))
        showPath()
    }
}
