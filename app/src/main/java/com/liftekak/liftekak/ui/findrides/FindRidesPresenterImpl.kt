package com.liftekak.liftekak.ui.findrides

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DatabaseReference
import com.google.gson.GsonBuilder
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.AskForRides
import com.liftekak.liftekak.services.AskForRidesAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

/**
 * Created by Sachinda on 3/3/2018.
 *
 */
class FindRidesPresenterImpl(var view: FindRidesContract.View?, val mContext: Context, val mDatabase: DatabaseReference, val mAuth: FirebaseAuth, private val dbUrl: String) : FindRidesContract.Presenter {

    private var childEventListener: ChildEventListener? = null
    private lateinit var findRidesList: ArrayList<AskForRides>

    override fun getRides(isProfileFrag: Int) {
        val findRidesService = findRidesAPIService()
        findRidesService.getRides(mAuth.currentUser?.uid as String)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete {
                    view?.hideLoadingIndicator()
                    view?.stopRefresh()
                }
                .subscribe(
                        { it -> loadRequests(it, isProfileFrag) },
                        { e ->
                            view?.showToast("Request could not be created. Please try again later.")
                            Timber.v(e, "Get Drives error: %s", e.localizedMessage)
                        })

    }

    override fun loadRequests(requestList: ArrayList<AskForRides>, isProfileFrag: Int) {
        findRidesList = ArrayList()
        if (requestList.isEmpty()) {
            view?.showToast("There are no ride requests...")
        } else {
            val storedUserId = Prefs(mContext).getUserId()
            for (request in requestList) {
                val requestTemp = replaceCharsForDisplay(request)
                if (isProfileFrag == 0) {
                    view?.addNewRequestsRV(requestTemp)
                } else {
                    if (requestTemp.userId.toInt() == storedUserId) {
                        view?.addNewRequestsRV(requestTemp)
                    }
                }
                findRidesList.add(requestTemp)
            }
        }
    }

    override fun filterRides(query: String): ArrayList<AskForRides> {
        val requests = getRidesList()
        val queryToLower = query.toLowerCase()

        val filteredRequestList = ArrayList<AskForRides>()
        for (request in requests) {
            val text = request.fromName.toLowerCase() + " to " + request.toName.toLowerCase()
            if (text.contains(queryToLower)) {
                filteredRequestList.add(request)
            }
        }
        return filteredRequestList
    }

    /**
     * Observable based retrofit service for drives
     */
    private fun findRidesAPIService(): AskForRidesAPI {

        val gSon = GsonBuilder()
                .setLenient()
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(dbUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gSon))
                .build()

        return retrofit.create(AskForRidesAPI::class.java)
    }

    private fun replaceCharsForDisplay(request: AskForRides): AskForRides {
        val fromName = request.fromName.replace('%', '\\').replace('*', '\'')
        val fromAddress = request.fromAddress.replace('%', '\\').replace('*', '\'')
        val toName = request.toName.replace('%', '\\').replace('*', '\'')
        val toAddress = request.toAddress.replace('%', '\\').replace('*', '\'')
        val polyline = request.polyline.replace('%', '\\').replace('*', '\'')
        request.fromName = fromName
        request.fromAddress = fromAddress
        request.toName = toName
        request.toAddress = toAddress
        request.polyline = polyline
        return request
    }

    override fun getRidesList(): List<AskForRides> {
        return findRidesList.reversed()
    }

    override fun onDestroy() {
        view = null
        childEventListener = null
    }
}