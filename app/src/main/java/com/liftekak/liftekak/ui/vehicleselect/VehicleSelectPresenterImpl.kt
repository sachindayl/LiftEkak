package com.liftekak.liftekak.ui.vehicleselect

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.liftekak.liftekak.base.constants.DBConstants
import java.util.*

/**
 * Created by Sachinda on 2/11/2018.
 */
class VehicleSelectPresenterImpl(val mContext: Context, val mDatabase: DatabaseReference, val mAuth: FirebaseAuth): VehicleSelectContract.Presenter{
    override fun writeVehicleToDB(make: String, model: String, year: String, color: String, plate: String, passengers: String) {
        val childUpdates = HashMap<String, Any?>()
        val user = mAuth.currentUser
        childUpdates["/${DBConstants.user_vehicles}/${user?.uid}/make"] = make
        childUpdates["/${DBConstants.user_vehicles}/${user?.uid}/model"] = model
        childUpdates["/${DBConstants.user_vehicles}/${user?.uid}/year"] = year
        childUpdates["/${DBConstants.user_vehicles}/${user?.uid}/color"] = color
        childUpdates["/${DBConstants.user_vehicles}/${user?.uid}/plate"] = plate
        childUpdates["/${DBConstants.user_vehicles}/${user?.uid}/passengers"] = passengers
        mDatabase.updateChildren(childUpdates)
    }
}