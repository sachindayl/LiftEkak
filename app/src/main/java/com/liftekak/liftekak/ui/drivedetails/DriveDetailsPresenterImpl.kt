package com.liftekak.liftekak.ui.drivedetails

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.liftekak.liftekak.base.GMapSupport
import com.liftekak.liftekak.base.PricingModel
import com.liftekak.liftekak.services.RetrofitService
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.OfferRidesRequests
import com.liftekak.liftekak.models.dbModels.Users
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

/**
 * Created by Sachinda on 3/18/2018.
 *
 */
class DriveDetailsPresenterImpl(val view: DriveDetailsContract.View, val mContext: Context, val mDatabase: DatabaseReference, val mAuth: FirebaseAuth, val dbUrl: String) : DriveDetailsContract.Presenter {
    override fun decodePolyline(polyline: String) {
        val gMapSupport = GMapSupport()
        var path = ArrayList<ArrayList<LatLng>>()
        var bounds = ArrayList<LatLngBounds>()
        val polyLineList = ArrayList<String>()
        polyLineList.add(polyline)
        Single.fromCallable {
            path = gMapSupport.decodePolyLine(polyLineList)
            bounds = gMapSupport.createDirectionBounds(path)
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { _ -> showPath(path, bounds) },
                        { e -> Timber.v(e, "Get Drives error: %s", e.localizedMessage) })
    }

    /**
     * Gets the driver name from the db if the userId is different from the stored user id
     */
    override fun driverDetails(userId: String, isMyProfile: Int) {
        val userForId: Users? = Users()
        val storedUserId = Prefs(mContext).getUserId()

        if ((userId.toInt() == storedUserId) || isMyProfile == 1) {
            userForId?.firstName = "You"
            userForId?.lastName = ""
            view.showDriverDetails(userForId!!)
            view.showMyProfileButtons()
        } else {
            val retrofit = RetrofitService().usersAPIService(dbUrl)
            retrofit.getCustomerName(userId.toInt())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { it ->
                                userForId?.firstName = it.firstName
                                userForId?.lastName = it.lastName
                                view.showDriverDetails(userForId!!)
                                view.showDriveProfileButtons()
                            },
                            { e ->
                                Timber.v(e, "Delete Request error: %s", e.localizedMessage)
                            })

        }

    }

    private fun showPath(path: ArrayList<ArrayList<LatLng>>, bounds: ArrayList<LatLngBounds>) {
        view.showPath(path, bounds)
    }

    override fun calculateCost(distance: String, passengers: String) {
        val pricing = PricingModel()
        val amount = pricing.calculateCost(distance, passengers, mContext)
        view.showCost(amount)
    }

    override fun requestDrive(requestId: Int) {
        val retrofit = RetrofitService().offerRidesAPIService(dbUrl)
        val userId = Prefs(mContext).getUserId()
        retrofit.insertDriveRequest(requestId, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete { view.disableRequestButton() }
                .subscribe({ it ->
                    view.showToast(it.message as String)
                    view.gotoTabFrag()
                }, { e ->
                    Timber.v(e, "Insert drive request error: %s", e.localizedMessage)
                })
    }

    override fun deleteRequest(requestId: Int) {
        val retrofit = RetrofitService().offerRidesAPIService(dbUrl)
        retrofit.deleteDrive(requestId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ s ->
                    view.showToast(s.message as String)
                    view.gotoTabFrag()
                }, { e ->
                    Timber.v(e, "Delete Request error: %s", e.localizedMessage)
                })
    }

    override fun isDriveRequested(requestId: Int) {
        val userId = Prefs(mContext).getUserId()
        var requestDetails :OfferRidesRequests
        RetrofitService().offerRidesAPIService(dbUrl)
                .getDriveRequestDetails(userId, requestId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it ->
                    requestDetails = it
                    Timber.v("Drive Request Details: %s", requestDetails)
                    if(requestDetails.requestId != 0 && requestDetails.requestUserId != 0){
                        view.disableRequestButton()
                    }
                },
                        { e -> Timber.v(e, "Get request details error: %s", e.localizedMessage) })


    }
}