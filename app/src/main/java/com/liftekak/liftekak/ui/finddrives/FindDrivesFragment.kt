package com.liftekak.liftekak.ui.finddrives


import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTabHost
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.adapters.FindDrivesRecyclerAdapter
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.OfferRides
import com.liftekak.liftekak.ui.askforride.AskForRideFragment
import com.liftekak.liftekak.ui.offerride.OfferRideFragment
import kotlinx.android.synthetic.main.fragment_find_drives.*


/**
 * A simple [Fragment] subclass.
 */
class FindDrivesFragment : BaseTabFragment(), FindDrivesContract.View {

    private lateinit var mPrefs: Prefs
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var presenter: FindDrivesContract.Presenter
    private lateinit var requestList: ArrayList<OfferRides>
    private lateinit var adapter: FindDrivesRecyclerAdapter
    private lateinit var rvFd: RecyclerView
    private var isProfileBundle: Bundle? = null
    private var isProfileFrag: Int? = null
    private var recyclerViewState: Parcelable? = null
    private var mTabHost: FragmentTabHost? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_find_drives, container, false)
        isProfileBundle = arguments
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        tabAct.supportActionBar?.title = "Lift Ekak"
        mTabHost = tabAct.findViewById(android.R.id.tabhost)
        rvFd = tabAct.findViewById(R.id.rv_fd)
        linearLayoutManager = LinearLayoutManager(tabAct)
        rvFd.layoutManager = linearLayoutManager
        mPrefs = Prefs(mContext)
        isProfileFrag = isProfileBundle?.getInt("isProfileFrag")
        setupActionButtons()
        requestList = ArrayList()
        adapter = FindDrivesRecyclerAdapter(requestList, isProfileFrag as Int, dbUrl)
        rvFd.adapter = adapter
        presenter = FindDrivesPresenterImpl(this, mDatabase, mAuth, mContext, dbUrl)

        sr_fd.setOnRefreshListener {
            requestList = ArrayList()
            adapter = FindDrivesRecyclerAdapter(requestList, isProfileFrag as Int,dbUrl)
            rvFd.adapter = adapter
            showLoadingIndicator()
            presenter.getDrives(isProfileFrag as Int)
        }

    }

    override fun setupActionButtons() {
        if (isProfileFrag == 0) {
            fab_fd_action_request_ride.setOnClickListener {
                mTabHost = null
                val rideRequestFrag = AskForRideFragment()
                tabAct.supportFragmentManager.beginTransaction()
                        .replace(R.id.main_tab_container, rideRequestFrag)
                        .addToBackStack("Ask For A Ride")
                        .commit()
            }

            fab_fd_action_offer_ride.setOnClickListener {
                val offerRidesFrag = OfferRideFragment()
                tabAct.supportFragmentManager.beginTransaction()
                        .replace(R.id.main_tab_container, offerRidesFrag)
                        .addToBackStack("Offer A Ride")
                        .commit()
            }
        } else {
            fab_fd_multiple_actions_rides.visibility = View.GONE
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        val search = menu?.findItem(R.id.action_search)
        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Search Drives..."
        search.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM or MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val filteredRequestList = presenter.filterDrives(newText as String)
                adapter.animateTo(filteredRequestList)
                rvFd.scrollToPosition(0)
                return true
            }
        })

        search.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                adapter.animateTo(requestList)
                rvFd.scrollToPosition(0)
                return true
            }
        })
    }


    override fun addNewRequestsRV(request: OfferRides) {
        if (!requestList.contains(request)) {
            requestList.add(0, request)
            rvFd.adapter.notifyItemInserted(requestList.size)
//            Timber.v("Adapter Size at addNew drives: %s", adapter.itemCount)
        }

    }

    override fun removeRequestsRV(request: OfferRides) {
        adapter.deleteItem(request)
    }

    override fun stopRefresh() {
        if (sr_fd != null && sr_fd.isRefreshing) {
            sr_fd.isRefreshing = false
        }
    }

    override fun showLoadingIndicator() {
        pb_fd?.visibility = View.VISIBLE
    }

    override fun hideLoadingIndicator() {
        pb_fd?.visibility = View.INVISIBLE
    }

    override fun onStart() {
        super.onStart()
        showLoadingIndicator()
        presenter.getDrives(isProfileFrag as Int)
        rvFd.layoutManager.onRestoreInstanceState(recyclerViewState)
    }

    override fun onStop() {
        super.onStop()
        recyclerViewState = rvFd.layoutManager.onSaveInstanceState()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
