package com.liftekak.liftekak.ui.tabhost


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTabHost
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.LiftEkakApplication
import com.liftekak.liftekak.ui.finddrives.FindDrivesFragment
import com.liftekak.liftekak.ui.findrides.FindRidesFragment
import kotlinx.android.synthetic.main.app_bar_tab.*


/**
 * A simple [Fragment] subclass.
 */
class TabHostFragment : BaseTabFragment() {


    private var mTabHost: FragmentTabHost? = null
    private var isProfileBundle: Bundle? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_tab_host, container, false)
        isProfileBundle = arguments
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct.appbar_tab_layout.visibility = View.VISIBLE
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        mTabHost = tabAct.findViewById(android.R.id.tabhost)
        mTabHost?.setup(tabAct, childFragmentManager, android.R.id.tabcontent)

        var isProfile = isProfileBundle?.getInt("isProfileFrag")
        if (isProfile == null) {
            isProfile = 0
        }
        val bundle = Bundle()
        bundle.putInt("isProfileFrag", isProfile)

        val driveTitle: String
        val rideTitle: String
        if (isProfile == 0) {
            driveTitle = "Drives"
            rideTitle = "Rides"
        } else {
            driveTitle = "My Drives"
            rideTitle = "My Rides"
        }

        mTabHost!!.addTab(mTabHost!!.newTabSpec("Drives")!!.setIndicator(driveTitle),
                FindDrivesFragment::class.java, bundle)
        mTabHost!!.addTab(mTabHost!!.newTabSpec("Rides")!!.setIndicator(rideTitle),
                FindRidesFragment::class.java, bundle)
        mTabHost!!.tabWidget.setBackgroundColor(ContextCompat.getColor(LiftEkakApplication.applicationContext(), R.color.colorPrimary))
        (0 until mTabHost!!.tabWidget.childCount)
                .map { mTabHost!!.tabWidget.getChildAt(it)!!.findViewById(android.R.id.title) as TextView }
                .forEach { it.setTextColor(ContextCompat.getColor(LiftEkakApplication.applicationContext(), R.color.white)) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mTabHost = null
    }
}// Required empty public constructor
