package com.liftekak.liftekak.ui.askforride

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.liftekak.liftekak.base.GMapSupport
import com.liftekak.liftekak.services.RetrofitService
import com.liftekak.liftekak.models.dbModels.AskForRides
import com.liftekak.liftekak.models.dbModels.ResponseModel
import com.liftekak.liftekak.models.directions.Directions
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * Created by Sachinda on 2/6/2018.
 *
 */
class AskForRidePresenterImpl(var view: AskForRideContract.View?, val mContext: Context, val mAuth: FirebaseAuth, val mDatabase: DatabaseReference, private val dbUrl: String) : AskForRideContract.Presenter {

    var routesList = ArrayList<String>()
    private var askForRidesCall: Disposable? = null
    private var distanceCall: Disposable? = null


    /**
     * Create a passenger request in the DB
     */
    override fun createRideRequest(requestRide: AskForRides) {
        val userId = mAuth.currentUser?.uid as String
        requestRide.userId = userId
        val askForRideTemp = replaceCharsForStorage(requestRide)
        val askForRidesService = RetrofitService().askForRidesAPIService(dbUrl)
        askForRidesCall = askForRidesService.insertRide(
                askForRideTemp.fromName, askForRideTemp.fromAddress,
                askForRideTemp.fromLatitude, askForRideTemp.fromLongitude, askForRideTemp.toName,
                askForRideTemp.toAddress, askForRideTemp.toLatitude, askForRideTemp.toLongitude,
                askForRideTemp.phoneNumber, askForRideTemp.date, askForRideTemp.time, askForRideTemp.recurrence,
                askForRideTemp.polyline, askForRideTemp.userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete { view?.goToTabFrag() }
                .subscribe(this::showResponse, this::handleError)

    }

    private fun showResponse(responseModel: ResponseModel) {
        view?.showToast(responseModel.message as String)
    }

    private fun handleError(error: Throwable) {
        view?.showToast("Request could not be created. Please try again later.")
        Timber.v(error, "insertDrive error: %s", error.localizedMessage)
    }


    /**
     * Checks if from and to boxes are empty
     */
    override fun checkForDistanceValues(from: String, to: String): Boolean {
        var distanceAvailable = false
        if (from != "" && to != "") distanceAvailable = true
        return distanceAvailable
    }

    override fun findRoutes(fromLatLng: LatLng, toLatLng: LatLng, key: String) {


        val directionsApi = RetrofitService().mapService()

        val units = "metric"
        val origin = "${fromLatLng.latitude},${fromLatLng.longitude}"
        val destination = "${toLatLng.latitude},${toLatLng.longitude}"
        try {
            distanceCall = directionsApi.getDirections(units, origin, destination, true, key)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { view?.showLoadingIndicator() }
                    .doOnComplete { view?.hideLoadingIndicator() }
                    .subscribe({ s -> routesOrganizer(s) }, { e ->
                        view?.showToast("Routes could not be found. Please try again later.")
                        Timber.v(e, "Get Drives error: %s", e.localizedMessage)
                    })

        } catch (e: Exception) {
            Timber.v("Error: %s", e.message)
        }
    }

    private fun routesOrganizer(directionsResponse: Directions) {
        val directions: Directions = directionsResponse
        val gMapSupport = GMapSupport()
        var bounds = ArrayList<LatLngBounds>()
        var path = ArrayList<ArrayList<LatLng>>()
        routesList.clear()
        val numOfRoutes = directions.routes?.size
        for (index in 0 until numOfRoutes!!) {
            val polyLine = directions.routes?.get(index)?.overviewPolyline?.points as String
            routesList.add(polyLine)
        }

        Single.fromCallable {
            path = gMapSupport.decodePolyLine(routesList)
            bounds = gMapSupport.createDirectionBounds(path)
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { view?.showPolyline(path, bounds) }
                .subscribe()
    }

    override fun pickRoute(index: String): String {
        val i = index.toInt()
        return routesList[i]
    }

    private fun replaceCharsForStorage(request: AskForRides): AskForRides {
        val fromName = request.fromName.replace('\\', '%').replace('\'', '*')
        val fromAddress = request.fromAddress.replace('\\', '%').replace('\'', '*')
        val toName = request.toName.replace('\\', '%').replace('\'', '*')
        val toAddress = request.toAddress.replace('\\', '%').replace('\'', '*')
        val polyline = request.polyline.replace('\\', '%').replace('\'', '*')
        request.fromName = fromName
        request.fromAddress = fromAddress
        request.toName = toName
        request.toAddress = toAddress
        request.polyline = polyline
        return request
    }

    override fun onDestroy() {
        view = null
        if (askForRidesCall?.isDisposed == false) askForRidesCall?.dispose()
        if (distanceCall?.isDisposed == false) distanceCall?.dispose()
    }

}
