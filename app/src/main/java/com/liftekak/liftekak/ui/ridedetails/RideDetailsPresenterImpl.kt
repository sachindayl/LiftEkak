package com.liftekak.liftekak.ui.ridedetails

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.liftekak.liftekak.base.GMapSupport
import com.liftekak.liftekak.services.RetrofitService
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.Users
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

/**
 * Created by Sachinda on 4/2/2018.
 */
class RideDetailsPresenterImpl(val view: RideDetailsContract.View?, val mContext: Context, val mDatabase: DatabaseReference, val mAuth: FirebaseAuth, val dbUrl: String) : RideDetailsContract.Presenter {

    override fun decodePolyline(polyline: String) {
        val gMapSupport = GMapSupport()
        var path = ArrayList<ArrayList<LatLng>>()
        var bounds = ArrayList<LatLngBounds>()
        val polyLineList = ArrayList<String>()
        polyLineList.add(polyline)
        Single.fromCallable {
            path = gMapSupport.decodePolyLine(polyLineList)
            bounds = gMapSupport.createDirectionBounds(path)
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { _ -> showPath(path, bounds) },
                        { e -> Timber.v(e, "Get Drives error: %s", e.localizedMessage) })
    }

    /**
     * Gets user id from the preferences to check if its the account owner or not
     */
    override fun riderDetails(userId: String, isMyProfile: Int) {
        val userForId: Users? = Users()
        val storedUserId = Prefs(mContext).getUserId()
        if ((userId.toInt() == storedUserId) || isMyProfile == 1) {
            userForId?.firstName = "You"
            userForId?.lastName = ""
            view?.showRiderDetails(userForId!!)
            view?.showMyProfileButtons()
        }
    }

    /**
     * Deletes Ride request from the DB
     */
    override fun deleteRequest(requestId: Int) {
        val retrofit = RetrofitService().askForRidesAPIService(dbUrl)
        retrofit.deleteRide(requestId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { s ->
                            view?.showToast(s.message as String)
                            view?.gotoTabFrag()
                        },
                        { e ->
                            Timber.v(e, "Delete Request error: %s", e.localizedMessage)
                        })
    }

    private fun showPath(path: ArrayList<ArrayList<LatLng>>, bounds: ArrayList<LatLngBounds>) {
        view?.showPath(path, bounds)
    }
}