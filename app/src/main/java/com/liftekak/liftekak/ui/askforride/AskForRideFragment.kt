package com.liftekak.liftekak.ui.askforride


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.AskForRides
import kotlinx.android.synthetic.main.fragment_ask_for_rides.*
import kotlinx.android.synthetic.main.fragment_offer_rides.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList


/**
 * This class is for Users to request rides
 */
class AskForRideFragment : BaseTabFragment(), AskForRideContract.View, OnMapReadyCallback, GoogleMap.OnPolylineClickListener {
    lateinit var spnRecurrence: String
    private lateinit var placeFromAddress: String
    private lateinit var placeFromName: String
    private lateinit var placeFromLatLng: LatLng
    private lateinit var placeToName: String
    private lateinit var placeToAddress: String
    private lateinit var placeToLatLng: LatLng
    private lateinit var mPrefs: Prefs
    private lateinit var gMap: GoogleMap
    private lateinit var rideRequest: AskForRides
    private lateinit var presenter: AskForRideContract.Presenter
    private lateinit var key: String
    private lateinit var selectedPolyline: String
    private lateinit var polylineList: ArrayList<Polyline>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ask_for_rides, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct.supportActionBar!!.title = "Ask For A Ride"
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter = AskForRidePresenterImpl(this, mContext, mAuth, mDatabase, dbUrl)
        val mapFragment = this.childFragmentManager.findFragmentById(R.id.map_ar) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mPrefs = Prefs(mContext)
        rideRequest = AskForRides()
        key = resources.getString(R.string.google_services_api_server_key)
        placeToName = ""
        placeToAddress = ""
        placeFromName = ""
        placeFromAddress = ""
        selectedPolyline = ""
        polylineList = ArrayList()

        showNumber()
        spinnerSetup()
        placesAutoCompleteSetup()
        dateTimePickerSetup()

        btn_ar_ride.setOnClickListener {
            if (fieldsNotEmpty()) {
                if (selectedPolyline == "") {
                    showSnackBar(cl_or, "Please select your preferred route.")
                } else {
                    mPrefs.setPhone(et_rr_phone_number.text.toString())
                    rideRequest.fromName = placeFromName
                    rideRequest.fromAddress = placeFromAddress
                    rideRequest.fromLatitude = placeFromLatLng.latitude
                    rideRequest.fromLongitude = placeFromLatLng.longitude
                    rideRequest.toName = placeToName
                    rideRequest.toAddress = placeToAddress
                    rideRequest.toLatitude = placeToLatLng.latitude
                    rideRequest.toLongitude = placeToLatLng.longitude
                    rideRequest.recurrence = spnRecurrence
                    rideRequest.date = et_rr_date.text.toString()
                    rideRequest.time = et_rr_time.text.toString()
                    rideRequest.phoneNumber = et_rr_phone_number.text.toString()
                    rideRequest.polyline = selectedPolyline
                    presenter.createRideRequest(rideRequest)
                }
            } else {
                showSnackBar(cl_ar, "Please fill all the fields.")
            }
        }
    }

    override fun spinnerSetup() {
        val timePeriodList = ArrayList<String>()
        timePeriodList.add("Recurrence")
        timePeriodList.add("Once")
        timePeriodList.add("Weekdays")
        timePeriodList.add("Weekends")
        val timePeriodAdapter = ArrayAdapter(mContext, R.layout.spinner_item, timePeriodList)
        spn_rr_time_period.adapter = timePeriodAdapter
        spn_rr_time_period.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                spnRecurrence = parent?.getItemAtPosition(pos).toString()
            }
        }
    }

    override fun placesAutoCompleteSetup() {
        val filter = AutocompleteFilter.Builder()
                .setCountry("LK")
                .build()

        val rideRequestFromFrag = this.childFragmentManager.findFragmentById(R.id.frag_ar_places_from) as SupportPlaceAutocompleteFragment
        rideRequestFromFrag.view?.findViewById<EditText>(R.id.place_autocomplete_search_input)?.textSize = 16f
        rideRequestFromFrag.setHint("From")
        rideRequestFromFrag.setFilter(filter)
        rideRequestFromFrag.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place?) {
                Timber.v("Place Selected From: %s", place?.latLng)
                placeFromName = place?.name as String
                placeFromAddress = place.address as String
                placeFromLatLng = place.latLng
                val distanceAvailabilityCheck = presenter.checkForDistanceValues(placeFromAddress, placeToAddress)
                if (distanceAvailabilityCheck) presenter.findRoutes(placeFromLatLng, placeToLatLng, key)
            }

            override fun onError(error: Status?) {
                Timber.i("Error Occurred: %s", error)
                showToast("Error occurred: ${error?.statusMessage}")
            }
        })

        val rideRequestToFrag = this.childFragmentManager.findFragmentById(R.id.frag_ar_places_to) as SupportPlaceAutocompleteFragment
        rideRequestToFrag.view?.findViewById<EditText>(R.id.place_autocomplete_search_input)?.textSize = 16f
        rideRequestToFrag.setHint("To")
        rideRequestToFrag.setFilter(filter)
        rideRequestToFrag.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place?) {
                Timber.v("Place Selected To: %s", place?.name)
                placeToName = place?.name as String
                placeToAddress = place.address as String
                placeToLatLng = place.latLng
                val distanceAvailabilityCheck = presenter.checkForDistanceValues(placeFromAddress, placeToAddress)
                if (distanceAvailabilityCheck) presenter.findRoutes(placeFromLatLng, placeToLatLng, key)

            }

            override fun onError(error: Status?) {
                showToast("Error occurred: ${error?.statusMessage}")
            }
        })
    }

    override fun dateTimePickerSetup() {
        et_rr_date.setOnClickListener {
            val mCurrentDate = Calendar.getInstance()
            val mYear = mCurrentDate.get(Calendar.YEAR)
            val mMonth = mCurrentDate.get(Calendar.MONTH)
            val mDay = mCurrentDate.get(Calendar.DAY_OF_MONTH)
            val mDatePicker = DatePickerDialog(tabAct, R.style.AppTheme_DialogTheme, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
                val dateSet = "$selectedDay/$selectedMonth/$selectedYear"
                et_rr_date.setText(dateSet)
            }, mYear, mMonth, mDay)
            mDatePicker.datePicker.minDate = System.currentTimeMillis() - 1000
            mDatePicker.setTitle("Select Date")
            mDatePicker.show()
        }

        et_rr_time.setOnClickListener {
            val mCurrentTime = Calendar.getInstance()
            val mHour = mCurrentTime.get(Calendar.HOUR_OF_DAY)
            val mMinute = mCurrentTime.get(Calendar.MINUTE)
            val mTimePicker = TimePickerDialog(tabAct, R.style.AppTheme_DialogTheme, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
                val clock12 = ((selectedHour + 11) % 12) + 1
                val amPm = if (selectedHour > 11) "PM"
                else "AM"
                val hour = when (clock12) {
                    in 1..9 -> "0$clock12"
                    else -> clock12.toString()
                }
                val minute = when (selectedMinute) {
                    0 -> "00"
                    in 1..9 -> "0$selectedMinute"
                    else -> selectedMinute.toString()
                }
                Timber.v("Offer Ride Time: %s:%s", selectedHour, selectedMinute)
                val timeSet = "$hour:$minute $amPm"
                et_rr_time.setText(timeSet)
            }, mHour, mMinute, false)
            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }
    }

    override fun showNumber() {
        et_rr_phone_number.setText(mPrefs.getPhone())
    }

    override fun goToTabFrag() {
        val fm = tabAct.supportFragmentManager
        fm.popBackStack()
//        fm.beginTransaction()
//                .replace(R.id.main_tab_container, TabHostFragment())
//                .addToBackStack("Tab Host")
//                .commit()
    }

    override fun showPolyline(polyLineSequenceList: ArrayList<ArrayList<LatLng>>, polyLineBoundsList: ArrayList<LatLngBounds>) {
        gMap.clear()
        polylineList.clear()
        for (i in 0 until polyLineSequenceList.size) {
            val polylineOpt = PolylineOptions()
            polylineOpt.clickable(true)
            polyLineSequenceList[i].forEach { points -> polylineOpt.add(points) }
            val polyline = gMap.addPolyline(polylineOpt)
            polyline.tag = i.toString()
            polylineList.add(polyline)
            stylePolylines()
            gMap.addMarker(MarkerOptions().position(polyLineSequenceList[i][0]))
            gMap.addMarker(MarkerOptions().position(polyLineSequenceList[i][polyLineSequenceList[i].lastIndex]))
        }

        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(polyLineBoundsList[0], 0)
        gMap.setPadding(5, 5, 5, 5)
        gMap.animateCamera(cameraUpdate)
        gMap.setOnPolylineClickListener(this)
    }

    override fun fieldsNotEmpty(): Boolean {
        val pfa = placeFromAddress != "" && placeFromAddress != "From"
        val pta = placeToAddress != "" && placeToAddress != "To"
        val pn = et_rr_phone_number.text.toString() != ""
        val time = et_rr_time.text.toString() != ""
        val date = et_rr_date.text.toString() != ""
        val spnRec = spnRecurrence != "" && spnRecurrence != "Recurrence"
        return pfa && pta && pn && time && date && spnRec
    }

    override fun onPolylineClick(polyline: Polyline?) {
        stylePolylines()
        val tag = polyline?.tag
        polyline?.color = ContextCompat.getColor(mContext, R.color.blue600)
        polyline?.width = 16f
        selectedPolyline = presenter.pickRoute(tag as String)
    }

    override fun stylePolylines() {
        for (polyline in polylineList) {
            polyline.width = 10f
            when (polyline.tag) {
                "0" -> polyline.color = ContextCompat.getColor(mContext, R.color.purple300)
                "1" -> polyline.color = ContextCompat.getColor(mContext, R.color.indigo300)
                "2" -> polyline.color = ContextCompat.getColor(mContext, R.color.lightBlue300)
                "3" -> polyline.color = ContextCompat.getColor(mContext, R.color.teal300)
                "4" -> polyline.color = ContextCompat.getColor(mContext, R.color.lightGreen300)
                else -> polyline.color = ContextCompat.getColor(mContext, R.color.blueGray300)
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        gMap = googleMap as GoogleMap
        gMap.setMinZoomPreference(1.0f)
        gMap.setMaxZoomPreference(20.0f)
        val colombo = LatLng(6.927, 79.861)
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(colombo, 10f))
    }

    override fun showLoadingIndicator() {
        pb_ar.visibility = View.VISIBLE
    }

    override fun hideLoadingIndicator() {
        pb_ar.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
