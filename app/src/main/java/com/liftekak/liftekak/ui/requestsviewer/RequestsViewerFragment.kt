package com.liftekak.liftekak.ui.requestsviewer


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment

/**
 * A simple [Fragment] subclass.
 *
 */
class RequestsViewerFragment : BaseTabFragment(), RequestsViewContract.View {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_requests_viewer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        tabAct.supportActionBar?.title = "Requests"
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        val search = menu?.findItem(R.id.action_search)
        search?.isVisible = false
    }


}
