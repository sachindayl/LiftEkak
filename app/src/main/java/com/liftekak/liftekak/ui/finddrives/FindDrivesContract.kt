package com.liftekak.liftekak.ui.finddrives

import com.liftekak.liftekak.models.dbModels.OfferRides

/**
 * Created by Sachinda on 2/20/2018.
 *
 */
interface FindDrivesContract {
    interface View{
        fun setupActionButtons()
        fun addNewRequestsRV(request: OfferRides)
        fun removeRequestsRV(request: OfferRides)
        fun showToast(message: String)
        fun showLoadingIndicator()
        fun hideLoadingIndicator()
        fun stopRefresh()
    }

    interface Presenter{
        fun getDrives(isProfileFrag: Int)
        fun loadRequests(requestList: ArrayList<OfferRides>, isProfileFrag: Int)
        fun filterDrives(query: String): ArrayList<OfferRides>
        fun getDrivesList(): List<OfferRides>
        fun onDestroy()
    }
}