package com.liftekak.liftekak.ui.askforride

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.liftekak.liftekak.models.dbModels.AskForRides

/**
 * Created by Sachinda on 2/6/2018.
 * For Passengers that needs rides. They can post a request so that drivers can contact them
 */
interface AskForRideContract {
    interface View {
        fun spinnerSetup()
        fun placesAutoCompleteSetup()
        fun showNumber()
        fun fieldsNotEmpty(): Boolean
        fun showToast(message: String)
        fun goToTabFrag()
        fun dateTimePickerSetup()
        fun showPolyline(polyLineSequenceList: ArrayList<ArrayList<LatLng>>,
                         polyLineBoundsList: ArrayList<LatLngBounds>)
        fun stylePolylines()
        fun showLoadingIndicator()
        fun hideLoadingIndicator()
    }

    interface Presenter {
        fun createRideRequest(requestRide: AskForRides)
        fun checkForDistanceValues(from: String, to: String): Boolean
        fun findRoutes(fromLatLng: LatLng, toLatLng: LatLng, key: String)
        fun pickRoute(index: String): String
        fun onDestroy()
    }
}