package com.liftekak.liftekak.ui.findrides

import com.liftekak.liftekak.models.dbModels.AskForRides

/**
 * Created by Sachinda on 3/3/2018.
 */
interface FindRidesContract{
    interface View{
        fun setupListeners()
        fun addNewRequestsRV(request: AskForRides)
        fun removeRequestsRV(request: AskForRides)
        fun showToast(message: String)
        fun showLoadingIndicator()
        fun hideLoadingIndicator()
        fun stopRefresh()
    }

    interface Presenter{
//        fun watchForRides(isProfileFrag: Int)
        fun getRides(isProfileFrag: Int)
        fun loadRequests(requestList: ArrayList<AskForRides>, isProfileFrag: Int)
        fun filterRides(query: String): ArrayList<AskForRides>
        fun getRidesList(): List<AskForRides>
        fun onDestroy()
    }
}