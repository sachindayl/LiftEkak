package com.liftekak.liftekak.ui.drivedetails

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Polyline
import com.liftekak.liftekak.models.dbModels.Users

/**
 * Created by Sachinda on 3/18/2018.
 */
interface DriveDetailsContract {
    interface View {
        fun showPath(polyLineSequenceList: ArrayList<ArrayList<LatLng>>, polyLineBoundsList: ArrayList<LatLngBounds>)
        fun stylePolylines(polyline: Polyline)
        fun showDriverDetails(user: Users)
        fun showMyProfileButtons()
        fun showDriveProfileButtons()
        fun buttonSetup()
        fun showCost(amount: String)
        fun showToast(message: String)
        fun gotoTabFrag()
        fun disableRequestButton()
    }

    interface Presenter {
        fun decodePolyline(polyline: String)
        fun driverDetails(userId: String, isMyProfile: Int)
        fun deleteRequest(requestId: Int)
        fun requestDrive(requestId: Int)
        fun calculateCost(distance: String, passengers: String)
        fun isDriveRequested(requestId: Int)
    }
}