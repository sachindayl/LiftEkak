package com.liftekak.liftekak.ui.drivedetails


import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.LiftEkakApplication
import com.liftekak.liftekak.base.constants.MapConstants
import com.liftekak.liftekak.models.dbModels.OfferRides
import com.liftekak.liftekak.models.dbModels.Users
import com.liftekak.liftekak.models.parcels.PolylineParcel
import com.liftekak.liftekak.ui.mapview.MapViewFragment
import com.liftekak.liftekak.ui.requestsviewer.RequestsViewerFragment
import kotlinx.android.synthetic.main.fragment_drive_details.*


/**
 * A simple [Fragment] subclass.
 */
class DriveDetailsFragment : BaseTabFragment(), DriveDetailsContract.View, OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private var driveDetailsBundle: Bundle? = null
    private lateinit var gMap: GoogleMap
    private lateinit var presenter: DriveDetailsContract.Presenter
    private lateinit var detailsList: ArrayList<String>
    private lateinit var startMarkerOptions: MarkerOptions
    private lateinit var endMarkerOptions: MarkerOptions
    private lateinit var polylineOptions: PolylineOptions
    private lateinit var polyLineSequenceListCopy: ArrayList<ArrayList<LatLng>>
    private lateinit var polyLineBoundsListCopy: ArrayList<LatLngBounds>
    private var isProfileFrag: Int? = null
    private var buttonType: Int? = null
    private lateinit var driveRequest: OfferRides


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_drive_details, container, false)
        //Bundled in Find Rides Recycler Adapter
        driveDetailsBundle = arguments
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        tabAct.supportActionBar?.title = "Drive Details"
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter = DriveDetailsPresenterImpl(this, mContext, mDatabase, mAuth, dbUrl)
        val mapFragment = this.childFragmentManager.findFragmentById(R.id.map_dd) as SupportMapFragment
        mapFragment.getMapAsync(this)
        driveRequest = OfferRides()
        detailsListInit()
        tv_dd_from_location.text = driveRequest.fromName
        tv_dd_to_location.text = driveRequest.toName
        tv_dd_start_date.text = driveRequest.date
        tv_dd_time.text = driveRequest.time
        tv_dd_distance.text = driveRequest.distance
        tv_dd_passenger_type.text = driveRequest.passengerType
        tv_dd_recurrence.text = driveRequest.recurrence
        tv_dd_num_of_passengers.text = driveRequest.numberOfPassengers.toString()
        presenter.driverDetails(driveRequest.userId, isProfileFrag as Int)
        presenter.calculateCost(driveRequest.distance, driveRequest.numberOfPassengers.toString())

    }

    private fun detailsListInit() {
        detailsList = driveDetailsBundle?.getStringArrayList("driveDetails")!!
        driveRequest.requestId = detailsList[0].toInt()
        driveRequest.fromName = detailsList[1]
        driveRequest.fromAddress = detailsList[2]
        driveRequest.fromLatitude = detailsList[3].toDouble()
        driveRequest.fromLongitude = detailsList[4].toDouble()
        driveRequest.toName = detailsList[5]
        driveRequest.toAddress = detailsList[6]
        driveRequest.toLatitude = detailsList[7].toDouble()
        driveRequest.toLongitude = detailsList[8].toDouble()
        driveRequest.phoneNumber = detailsList[9]
        driveRequest.date = detailsList[10]
        driveRequest.time = detailsList[11]
        driveRequest.distance = detailsList[12]
        driveRequest.duration = detailsList[13]
        driveRequest.recurrence = detailsList[14]
        driveRequest.passengerType = detailsList[15]
        driveRequest.numberOfPassengers = detailsList[16].toInt()
        driveRequest.polyline = detailsList[17]
        driveRequest.userId = detailsList[18]
        isProfileFrag = detailsList[19].toInt()
        driveRequest.createdAt = detailsList[20]
    }

    /**
     * Button text and listeners are set according to buttonType. If 0, then its another user,
     * if 1, its user's own request
     */
    override fun buttonSetup() {
        if (buttonType == 0) {
            btn_dd_left.setOnClickListener {
                val number = driveRequest.phoneNumber
                val intent = Intent(Intent.ACTION_CALL)
                intent.data = Uri.parse("tel:$number")
                if (number == "") {
                    showSnackBar(cl_dd, "Number is not available.")
                } else {
                    val checkCallPermission = ContextCompat.checkSelfPermission(tabAct, android.Manifest.permission.CALL_PHONE)
                    if (checkCallPermission == PackageManager.PERMISSION_GRANTED) {
                        try {
                            startActivity(intent)
                        } catch (e: SecurityException) {
                            e.printStackTrace()
                            showSnackBar(cl_dd, "Permission to call has been denied by the user.")
                        }

                    } else {
                        ActivityCompat.requestPermissions(tabAct, arrayOf(android.Manifest.permission.CALL_PHONE), MapConstants.PERMISSIONS_REQUEST_CALL_PHONE)
                    }
                }
            }
            btn_dd_right.setOnClickListener {
                AlertDialog.Builder(tabAct)
                        .setTitle("Request Drive")
                        .setMessage("Are you sure you want to request this drive?")
                        .setPositiveButton("Yes") { _, _ ->
                            presenter.requestDrive(driveRequest.requestId)
                        }
                        .setNegativeButton("No", null)
                        .show()

            }

        } else {
            btn_dd_left.setOnClickListener {
                showToast("Requests Button Pressed")
                val fm = tabAct.supportFragmentManager
                fm.beginTransaction()
                        .replace(R.id.main_tab_container, RequestsViewerFragment())
                        .addToBackStack("Request Viewer")
                        .commit()
            }
            btn_dd_right.setOnClickListener {
                AlertDialog.Builder(tabAct)
                        .setTitle("Delete Drive")
                        .setMessage("Are you sure you want to delete this drive request?")
                        .setPositiveButton("Yes") { _, _ ->
                            presenter.deleteRequest(driveRequest.requestId)
                        }
                        .setNegativeButton("No", null)
                        .show()

            }
        }

    }

    override fun showCost(amount: String) {
        val amountValue = "Rs.$amount"
        tv_dd_cost.text = amountValue
    }

    /**
     * Creates the users path on Google Map
     */
    override fun showPath(polyLineSequenceList: ArrayList<ArrayList<LatLng>>, polyLineBoundsList: ArrayList<LatLngBounds>) {
        polyLineSequenceListCopy = polyLineSequenceList
        polyLineBoundsListCopy = polyLineBoundsList
        val polylineOpt = PolylineOptions()
        polyLineSequenceList[0].forEach { points -> polylineOpt.add(points) }
        gMap.clear()
        polylineOptions = polylineOpt
        val polyline = gMap.addPolyline(polylineOpt)
        val bounds: LatLngBounds = polyLineBoundsList[0]
        stylePolylines(polyline)
        startMarkerOptions = MarkerOptions().position(polyLineSequenceList[0][0])
        endMarkerOptions = MarkerOptions().position(polyLineSequenceList[0][polyLineSequenceList[0].lastIndex])
        gMap.addMarker(startMarkerOptions)
        gMap.addMarker(endMarkerOptions)
        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 10)
        gMap.animateCamera(cameraUpdate)
        gMap.uiSettings.isScrollGesturesEnabled = false
    }


    /**
     * Shows drivers name
     */
    override fun showDriverDetails(user: Users) {
        val driverName = "${user.firstName} ${user.lastName}"
        tv_dd_driver_name.text = driverName
    }

    override fun showMyProfileButtons() {
        buttonType = 1
        btn_dd_left.text = resources.getString(R.string.requests)
        btn_dd_right.text = resources.getString(R.string.delete_request)
        buttonSetup()
    }

    override fun showDriveProfileButtons() {
        buttonType = 0
        btn_dd_left.text = resources.getString(R.string.call_for_more_info)
        btn_dd_right.text = resources.getString(R.string.request_drive)
        buttonSetup()
    }

    override fun stylePolylines(polyline: Polyline) {
        polyline.width = 10f
        polyline.color = ContextCompat.getColor(mContext, R.color.purple300)
    }

    override fun onMapClick(p0: LatLng?) {
        val bundle = Bundle()
        bundle.putParcelable("mapParcel", PolylineParcel(polyLineSequenceListCopy, polyLineBoundsListCopy))
        val mapViewFragment = MapViewFragment()
        mapViewFragment.arguments = bundle
        tabAct.supportFragmentManager.beginTransaction()
                .replace(R.id.main_tab_container, mapViewFragment)
                .addToBackStack("Map View")
                .commit()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        gMap = googleMap as GoogleMap
        gMap.setMinZoomPreference(1.0f)
        gMap.setMaxZoomPreference(20.0f)
        val colombo = LatLng(6.927, 79.861)
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(colombo, 10f))
        presenter.decodePolyline(driveRequest.polyline)
        gMap.setOnMapClickListener(this)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        val search = menu?.findItem(R.id.action_search)
        search?.isVisible = false
    }

    override fun gotoTabFrag() {
        val fm = tabAct.supportFragmentManager
        fm.popBackStack()
    }

    override fun disableRequestButton() {
        btn_dd_right.text = resources.getString(R.string.drive_requested)
        btn_dd_right.isEnabled = false
        btn_dd_right.setTextColor(ContextCompat.getColor(LiftEkakApplication.applicationContext(), R.color.white50))
    }

    override fun onStart() {
        super.onStart()
        presenter.isDriveRequested(driveRequest.requestId)
    }
}
