package com.liftekak.liftekak.ui.requestsviewer

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.liftekak.liftekak.ui.offerride.OfferRideContract

/**
 * Created by Sachinda on 5/25/2018.
 */
class RequestsViewerPresenterImpl(var view: OfferRideContract.View?, val mContext: Context,
                                  val mDatabase: DatabaseReference, val mAuth: FirebaseAuth,
                                  private val dbUrl: String): RequestsViewContract.Presenter{

}