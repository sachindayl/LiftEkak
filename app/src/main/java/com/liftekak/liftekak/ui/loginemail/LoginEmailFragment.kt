package com.liftekak.liftekak.ui.loginemail


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseUser
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseFragment
import com.liftekak.liftekak.base.Notify
import com.liftekak.liftekak.base.RegexImpl
import com.liftekak.liftekak.interfaces.Regex
import com.liftekak.liftekak.models.dbModels.Users
import com.liftekak.liftekak.ui.loginselection.LoginSelectionFragment
import com.liftekak.liftekak.ui.main.TabActivity
import kotlinx.android.synthetic.main.fragment_login_email.*
import timber.log.Timber


/**
 * A simple [Fragment] subclass.
 */
class LoginEmailFragment : BaseFragment(), LoginEmailContract.View {

    lateinit var presenter: LoginEmailContract.Presenter
    lateinit var regexCheck: Regex
    private lateinit var notify: Notify

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        mainAct.appbar_layout.visibility = View.VISIBLE
//        mainAct.supportActionBar!!.title = "Register"
//        mainAct.supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        presenter = LoginEmailPresenterImpl(this, mContext, mAuth, mDatabase, databaseUrl)
        regexCheck = RegexImpl()
        notify = Notify(mainAct)
        setupButtons()
        checkRegEx()

    }

    override fun setupButtons() {
        tv_sign_in.setOnClickListener {
            goToSignIn()
        }
        btn_register.setOnClickListener {
            val noErrors = checkForErrors()
            if (noErrors) {
//                val encryptPass = Encrypt(mContext)
//                val encryptedPass = encryptPass.encrypt(et_register_confirm_password.text.toString())
                val firstName = et_register_first_name.text.toString()
                val lastName = et_register_last_name.text.toString()
                val password = et_register_confirm_password.text.toString()
                val email = et_register_email.text.toString()
                val phoneNumber = et_register_phone_number.text.toString()
                val newUser = Users(firstName, lastName, email, phoneNumber, password, "false", null)
                presenter.createUser(newUser)
            } else {
                notify.showSnackBar(cl_le, "Please fix the errors.")
            }
        }
    }

    /**
     * Checking for the standard in names, emails, numbers and passwords
     */
    override fun checkRegEx() {

        et_register_first_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val firstNameValid = regexCheck.isNameValid(s.toString())
                if (!firstNameValid)
                    et_register_first_name.error = "Please enter a correct first name"
                else
                    et_register_first_name.error = null
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        et_register_last_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val lastNameValid = regexCheck.isNameValid(s.toString())
                if (!lastNameValid) {
                    et_register_last_name.error = "Please enter a correct last name"
                } else {
                    et_register_last_name.error = null
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        et_register_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val emailValid = regexCheck.isEmailValid(s.toString())
                if (!emailValid) {
                    et_register_email.error = "Please enter a correct email"
                } else {
                    et_register_last_name.error = null
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        et_register_phone_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val phoneValid = regexCheck.isPhoneNumberValid(s.toString())
                if (!phoneValid) {
                    et_register_phone_number.error = "Please enter a correct phone number"
                } else {
                    et_register_phone_number.error = null
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        et_register_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val passwordValid = regexCheck.isPasswordValid(s.toString())
                if (!passwordValid) {
                    et_register_password.error = "Must contain at least:\nA minimum of 8 characters\nA digit\nA special character\nAn uppercase character"
                } else {
                    et_register_password.error = null
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        et_register_confirm_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val pass = et_register_password.text.toString()
                val confirmPass = et_register_confirm_password.text.toString()
                val passwordMatch = presenter.isPasswordMatching(pass, confirmPass)
                if (!passwordMatch) {
                    et_register_confirm_password.error = "Passwords does not match"
                } else {
                    et_register_confirm_password.error = null
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    /**
     * Goes to Sign in activity
     */
    override fun goToSignIn() {
        val fm = mainAct.supportFragmentManager
        fm.beginTransaction()
                .replace(R.id.main_container, LoginSelectionFragment())
                .addToBackStack("Sign In")
                .commit()

        Timber.v("Visited SignIn Fragment")
    }

    /**
     * Check if edit texts have errors before registering
     */
    override fun checkForErrors(): Boolean {
        var noErrors = false
        val firstNameCheck = et_register_first_name.error
        val lastNameCheck = et_register_last_name.error
        val emailCheck = et_register_email.error
        val phoneNumberCheck = et_register_phone_number.error
        val passwordCheck = et_register_password.error

        // TODO check for all empty strings
        if (firstNameCheck == null && lastNameCheck == null && emailCheck == null
                && phoneNumberCheck == null && passwordCheck == null)
            noErrors = true

        return noErrors
    }

    override fun gotoUserDashboard(user: FirebaseUser) {
//        val requestRideFrag = AskForRideFragment()
//        mainAct.supportFragmentManager.beginTransaction()
//                .replace(R.id.main_container, requestRideFrag)
//                .addToBackStack("Request A Ride")
//                .commit()

        startActivity(Intent(mainAct, TabActivity::class.java))
        mainAct.finish()
    }
}
