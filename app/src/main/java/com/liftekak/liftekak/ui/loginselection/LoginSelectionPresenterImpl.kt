package com.liftekak.liftekak.ui.loginselection

import android.os.Bundle
import com.facebook.AccessToken
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.liftekak.liftekak.base.constants.DBConstants
import timber.log.Timber
import java.util.*


/**
 * Created by Sachinda on 2/2/2018.
 *
 */
class LoginSelectionPresenterImpl(var view: LoginSelectionContract.View?, val mAuth: FirebaseAuth, val mDatabase: DatabaseReference) : LoginSelectionContract.Presenter {
    private var userId = ""
    override fun validateUserByEmail(email: String, password: String) {
        try {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            view?.goToUserDashboard()
                        } else {
                            // If sign in fails, display a message to the user.
                            Timber.v(task.exception, "createUserWithEmail:failure")
                            view?.showToast("Authentication failed. ${task.exception?.message.toString()}")

                        }
                    }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun validateUserByFacebook(token: AccessToken, fbBundle: Bundle) {

        val credential = FacebookAuthProvider.getCredential(token.token)

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        userId = mAuth.currentUser?.uid as String
                        if (!checkForUserAccountOnDB()) {
                            writeFbUserToDB(fbBundle)
                        }
                        view?.goToUserDashboard()

//                        view.changeFbButtonText()
                        view?.showToast("Authenticated!")
                    } else {
                        // If sign in fails, display a message to the user.
                        Timber.v(task.exception, "createUserWithEmail:failure")
                        view?.showToast("Authentication failed. ${task.exception?.message.toString()}")

                    }
                }
    }

    override fun checkForUserAccountOnDB(): Boolean {
        val ref: DatabaseReference = mDatabase.child(DBConstants.users)
        var alreadyAUser = false
        Timber.v("user id %s", userId)
        ref.orderByKey().equalTo(userId).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.children != null) {
                    alreadyAUser = true
                    Timber.v("Children count %s", dataSnapshot.childrenCount)

                }

//                for(users in dataSnapshot.children){
//                    if(users.key == user.uid){
//                        alreadyAUser = true
//                        break
//                    }
//                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Timber.e(databaseError.toException(), "DB Error: %s", databaseError.message)
            }
        })
        Timber.v("Already a user %s", alreadyAUser)
        return alreadyAUser
    }

    override fun writeFbUserToDB(fbBundle: Bundle) {
        val fbList = fbBundle.getStringArrayList("fbProfile")

        val childUpdates = HashMap<String, Any>()
        childUpdates["/${DBConstants.users}/$userId/first_name"] = fbList[0]
        childUpdates["/${DBConstants.users}/$userId/last_name"] = fbList[1]
        childUpdates["/${DBConstants.users}/$userId/email"] = fbList[2]
        childUpdates["/${DBConstants.users}/$userId/is_admin"] = false
        childUpdates["/${DBConstants.users}/$userId/created_at"] = ServerValue.TIMESTAMP
        mDatabase.updateChildren(childUpdates)
    }

    override fun onDestroy() {
        view = null
    }
}