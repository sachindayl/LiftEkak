package com.liftekak.liftekak.ui.myprofile

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.liftekak.liftekak.services.RetrofitService
import com.liftekak.liftekak.base.constants.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import timber.log.Timber
import java.io.File

/**
 * Created by Sachinda on 4/5/2018.
 */
class MyProfilePresenterImpl(var view: MyProfileContract.View?, val mContext: Context, val mDatabase: DatabaseReference, val mAuth: FirebaseAuth, val dbUrl: String) : MyProfileContract.Presenter {

    private var userInfoCall: Disposable? = null
    private var imagePostCall: Disposable? = null

    override fun requestUserInfo() {
        val userId = Prefs(mContext).getUserId()
        val retrofit = RetrofitService().usersAPIService(dbUrl)
        userInfoCall = retrofit.getCustomerName(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view?.showLoadingIndicator() }
                .doOnComplete { view?.hideLoadingIndicator() }
                .subscribe({ it -> view?.showUserInfo(it) }, { e ->
                    view?.showToast("Request could not be created. Please try again later.")
                    Timber.v(e, "Get Drives error: %s", e.localizedMessage)
                })
    }

    override fun uploadImage(file: File) {
        val userId = Prefs(mContext).getUserId().toString()
        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val imageFileBody = MultipartBody.Part.createFormData("image", file.name, requestBody)
        Timber.v("Image Name: %s", file.name)
        imagePostCall = RetrofitService().imagesAPIService(dbUrl).uploadImage(imageFileBody, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view?.showLoadingIndicator() }
                .doOnComplete { view?.hideLoadingIndicator() }
                .subscribe({ s ->
                    view?.showToast(s.message as String)
                    Prefs(mContext).setUserImage(file.name)
                    view?.setProfileImage()
                }, { e ->
                    view?.showToast("Image could not be uploaded. Please try again later.")
                    Timber.v(e, "Image upload error: %s", e.localizedMessage)
                })
    }

    override fun onDestroy() {
        if (view != null) view = null
        if (!userInfoCall?.isDisposed!!) userInfoCall?.dispose()
        if (!imagePostCall?.isDisposed!!) imagePostCall?.dispose()
    }


}