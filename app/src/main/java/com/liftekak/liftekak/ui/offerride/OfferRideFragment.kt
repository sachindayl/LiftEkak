package com.liftekak.liftekak.ui.offerride


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.Notify
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.OfferRides
import kotlinx.android.synthetic.main.fragment_offer_rides.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class OfferRideFragment : BaseTabFragment(), OfferRideContract.View, OnMapReadyCallback, GoogleMap.OnPolylineClickListener {

    private lateinit var gMap: GoogleMap
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var presenter: OfferRideContract.Presenter
    private lateinit var mPrefs: Prefs
    private lateinit var placeFromName: String
    private lateinit var placeFromAddress: String
    private lateinit var placeFromLatLng: LatLng
    private lateinit var placeToName: String
    private lateinit var placeToAddress: String
    private lateinit var placeToLatLng: LatLng
    private lateinit var spnRecurrence: String
    private lateinit var spnPassengerType: String
    private lateinit var notify: Notify
    private lateinit var offerRequest: OfferRides
    private lateinit var key: String
    private lateinit var selectedPolyline: String
    private lateinit var polylineList: ArrayList<Polyline>
    private var distance: String? = ""
    private var duration: String? = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_offer_rides, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct.supportActionBar!!.title = "Offer A Ride"
        tabAct.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter = OfferRidePresenterImpl(this, mContext, mDatabase, mAuth, dbUrl)
        val mapFragment = this.childFragmentManager.findFragmentById(R.id.map_or) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(tabAct)
        mPrefs = Prefs(mContext)
        notify = Notify(mContext)
        offerRequest = OfferRides()
        key = resources.getString(R.string.google_services_api_server_key)
        placeToAddress = ""
        placeFromAddress = ""
        selectedPolyline = ""
        polylineList = ArrayList()

        showNumber()
        spinnerSetup()
        placesAutoCompleteSetup()
        dateTimePickerSetup()

        btn_or_ride.setOnClickListener {

            if (checkForEmptyFields()) {
                if (selectedPolyline == "") {
                    showSnackBar(cl_or, "Please select your preferred route.")
                } else {
                    mPrefs.setPhone(et_or_phone_number.text.toString())
                    offerRequest.fromName = placeFromName
                    offerRequest.fromAddress = placeFromAddress
                    offerRequest.fromLatitude = placeFromLatLng.latitude
                    offerRequest.fromLongitude = placeFromLatLng.longitude
                    offerRequest.toName = placeToName
                    offerRequest.toAddress = placeToAddress
                    offerRequest.toLatitude = placeToLatLng.latitude
                    offerRequest.toLongitude = placeToLatLng.longitude
                    offerRequest.phoneNumber = et_or_phone_number.text.toString()
                    offerRequest.date = et_or_date.text.toString()
                    offerRequest.time = et_or_time.text.toString()
                    offerRequest.distance = distance as String
                    offerRequest.duration = duration as String
                    offerRequest.recurrence = spnRecurrence
                    offerRequest.passengerType = spnPassengerType
                    offerRequest.polyline = selectedPolyline
                    presenter.createDriveRequest(offerRequest)
                }
            } else {
                showSnackBar(cl_or, "Please fill all the fields.")
            }
        }
    }

    override fun spinnerSetup() {
        val timePeriodList = ArrayList<String>()
        timePeriodList.add("Recurrence")
        timePeriodList.add("Once")
        timePeriodList.add("Weekdays")
        timePeriodList.add("Weekends")
        timePeriodList.add("Daily")
        timePeriodList.add("Weekly")
        val timePeriodAdapter = ArrayAdapter(mContext, R.layout.spinner_item, timePeriodList)
        spn_or_time_period.adapter = timePeriodAdapter
        spn_or_time_period.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                (p1 as TextView?)?.setTextColor(Color.BLACK)
                spnRecurrence = parent?.getItemAtPosition(pos).toString()
            }
        }

        val passengerTypeList = ArrayList<String>()
        passengerTypeList.add("Passenger type")
        passengerTypeList.add("Male & female passengers")
        passengerTypeList.add("Female passengers only")
        passengerTypeList.add("Male passengers only")

        val passengerTypeAdapter = ArrayAdapter(mContext, R.layout.spinner_item, passengerTypeList)
        spn_or_passengers.adapter = passengerTypeAdapter
        spn_or_passengers.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                (p1 as TextView?)?.setTextColor(Color.BLACK)
                spnPassengerType = parent?.getItemAtPosition(pos).toString()
            }
        }
    }

    /**
     * Uses date and time pickers to set date / time
     */
    override fun dateTimePickerSetup() {
        et_or_date.setOnClickListener {
            val mCurrentDate = Calendar.getInstance()
            val mYear = mCurrentDate.get(Calendar.YEAR)
            val mMonth = mCurrentDate.get(Calendar.MONTH)
            val mDay = mCurrentDate.get(Calendar.DAY_OF_MONTH)
            val mDatePicker = DatePickerDialog(tabAct, R.style.AppTheme_DialogTheme, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
                val dateSet = "$selectedDay/${selectedMonth + 1}/$selectedYear"
                et_or_date.setText(dateSet)
            }, mYear, mMonth, mDay)
            mDatePicker.datePicker.minDate = System.currentTimeMillis() - 1000
            mDatePicker.setTitle("Select Date")
            mDatePicker.show()
        }

        et_or_time.setOnClickListener {
            val mCurrentTime = Calendar.getInstance()
            val mHour = mCurrentTime.get(Calendar.HOUR_OF_DAY)
            val mMinute = mCurrentTime.get(Calendar.MINUTE)
            val mTimePicker = TimePickerDialog(tabAct, R.style.AppTheme_DialogTheme, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
                val clock12 = ((selectedHour + 11) % 12) + 1
                val amPm = if (selectedHour > 11) "PM"
                else "AM"
                val hour = when (clock12) {
                    in 1..9 -> "0$clock12"
                    else -> clock12.toString()
                }
                val minute = when (selectedMinute) {
                    0 -> "00"
                    in 1..9 -> "0$selectedMinute"
                    else -> selectedMinute.toString()
                }
                Timber.v("Offer Ride Time: %s:%s", selectedHour, selectedMinute)
                val timeSet = "$hour:$minute $amPm"
                et_or_time.setText(timeSet)
            }, mHour, mMinute, false)
            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }
    }

    /**
     * google place autocomplete for addresses setup
     */
    override fun placesAutoCompleteSetup() {
        val filter = AutocompleteFilter.Builder()
                .setCountry("LK")
                .build()

        val rideFromFrag = this.childFragmentManager.findFragmentById(R.id.frag_or_places_from) as SupportPlaceAutocompleteFragment
        rideFromFrag.view?.findViewById<EditText>(R.id.place_autocomplete_search_input)?.textSize = 16f
        rideFromFrag.setHint("From")
        rideFromFrag.setFilter(filter)
        rideFromFrag.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place?) {
                placeFromName = place?.name as String
                placeFromAddress = place.address as String
                placeFromLatLng = place.latLng
                val distanceAvailabilityCheck = presenter.checkForDistanceValues(placeFromAddress, placeToAddress)
                if (distanceAvailabilityCheck) {
                    presenter.findRoutes(placeFromLatLng, placeToLatLng, key)
                }
            }

            override fun onError(error: Status?) {
                notify.showToast("Error occurred: ${error?.statusMessage}")

            }
        })

        val rideToFrag = this.childFragmentManager.findFragmentById(R.id.frag_or_places_to) as SupportPlaceAutocompleteFragment
        rideToFrag.view?.findViewById<EditText>(R.id.place_autocomplete_search_input)?.textSize = 16f
        rideToFrag.setHint("To")
        rideToFrag.setFilter(filter)
        rideToFrag.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place?) {
                placeToName = place?.name as String
                placeToAddress = place.address as String
                placeToLatLng = place.latLng
                val distanceAvailabilityCheck = presenter.checkForDistanceValues(placeFromAddress, placeToAddress)
                if (distanceAvailabilityCheck) {
                    presenter.findRoutes(placeFromLatLng, placeToLatLng, key)
                }
            }

            override fun onError(error: Status?) {
                Timber.i("Error Occurred: %s", error?.statusMessage)
            }
        })
    }

    override fun goToTabFrag() {
        val fm = tabAct.supportFragmentManager
        fm.popBackStack()
//        fm.beginTransaction()
//                .replace(R.id.main_tab_container, TabHostFragment())
//                .addToBackStack("Tab Host")
//                .commit()
    }

    /**
     * Shows distance and duration to user
     */
    override fun showLocationInfo(distanceValue: String?, durationValue: String?) {
        val offerDistance = if (distanceValue == null && distanceValue == "") "N/A"
        else "$distanceValue"

        val offerDuration = if (durationValue == null && durationValue == "") "N/A"
        else "$durationValue"

        tv_or_distance.text = offerDistance
        tv_or_duration.text = offerDuration
    }

    /**
     * Shows all the polylines and colors them with different colors, also adds markers for
     * beginning and end
     */
    override fun showPolyline(polyLineSequenceList: ArrayList<ArrayList<LatLng>>, polyLineBoundsList: ArrayList<LatLngBounds>) {

        gMap.clear()
        polylineList.clear()
        for (i in 0 until polyLineSequenceList.size) {
            val polylineOpt = PolylineOptions()
            polylineOpt.clickable(true)
            polyLineSequenceList[i].forEach { points -> polylineOpt.add(points) }
            val polyline = gMap.addPolyline(polylineOpt)
            polyline.tag = i.toString()
            polylineList.add(polyline)
            stylePolylines()
            gMap.addMarker(MarkerOptions().position(polyLineSequenceList[i][0]))
            gMap.addMarker(MarkerOptions().position(polyLineSequenceList[i][polyLineSequenceList[i].lastIndex]))
        }


        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(polyLineBoundsList[0], 0)
        gMap.setPadding(5, 5, 5, 5)
        gMap.animateCamera(cameraUpdate)
        gMap.setOnPolylineClickListener(this)
    }

    override fun showNumber() {
        et_or_phone_number.setText(mPrefs.getPhone())
    }

    override fun checkForEmptyFields(): Boolean {
        val pfa = placeFromAddress != "" && placeFromAddress != "From"
        val pta = placeToAddress != "" && placeToAddress != "To"
        val pn = et_or_phone_number.text.toString() != "" && et_or_phone_number.text.toString() != "Phone number"
        val time = et_or_time.text.toString() != ""
        val date = et_or_date.text.toString() != ""
        val recSpn = spnRecurrence != "" && spnRecurrence != "Recurrence"
        val pasSpn = spnPassengerType != "" && spnPassengerType != "Passenger type"

        return pfa && pta && pn && time && date && recSpn && pasSpn
    }

    override fun onPolylineClick(polyline: Polyline?) {
        stylePolylines()
        val tag = polyline?.tag
        polyline?.color = ContextCompat.getColor(mContext, R.color.blue600)
        polyline?.width = 16f
        selectedPolyline = presenter.pickRoute(tag as String)
        distance = presenter.pickDistance(tag)
        duration = presenter.pickDuration(tag)
        showLocationInfo(distance, duration)
    }

    override fun stylePolylines() {
        for (polyline in polylineList) {
            polyline.width = 10f
            when (polyline.tag) {
                "0" -> polyline.color = ContextCompat.getColor(mContext, R.color.purple300)
                "1" -> polyline.color = ContextCompat.getColor(mContext, R.color.indigo300)
                "2" -> polyline.color = ContextCompat.getColor(mContext, R.color.lightBlue300)
                "3" -> polyline.color = ContextCompat.getColor(mContext, R.color.teal300)
                "4" -> polyline.color = ContextCompat.getColor(mContext, R.color.lightGreen300)
                else -> polyline.color = ContextCompat.getColor(mContext, R.color.blueGray300)
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        gMap = googleMap as GoogleMap
        gMap.setMinZoomPreference(1.0f)
        gMap.setMaxZoomPreference(20.0f)
        val colombo = LatLng(6.927, 79.861)
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(colombo, 10f))

    }

    override fun showLoadingIndicator() {
        pb_or?.visibility = View.VISIBLE
    }

    override fun hideLoadingIndicator() {
        pb_or?.visibility = View.GONE
    }
}
