package com.liftekak.liftekak.ui.finddrives

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.liftekak.liftekak.services.RetrofitService
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.OfferRides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * Created by Sachinda on 2/20/2018.
 *
 */
class FindDrivesPresenterImpl(var view: FindDrivesContract.View?, val mDatabase: DatabaseReference, val mAuth: FirebaseAuth, val mContext: Context, private val dbUrl: String) : FindDrivesContract.Presenter {

    private lateinit var findDrivesList: ArrayList<OfferRides>
    private var call: Disposable? = null

    /**
     * Gets Rides from the DB to create a Recycle View
     */
    override fun getDrives(isProfileFrag: Int) {

        val retrofit = RetrofitService().offerRidesAPIService(dbUrl)
        call = retrofit.getDrives(mAuth.currentUser?.uid as String)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete {
                    view?.hideLoadingIndicator()
                    view?.stopRefresh()
                }
                .subscribe(
                        { it -> loadRequests(it, isProfileFrag) },
                        { e ->
                            view?.showToast("Request could not be created. Please try again later.")
                            Timber.v(e, "Get Drives error: %s", e.localizedMessage)
                        })
    }

    /**
     * adding all the requests pulled from the db to the recycler view adapter
     * also, makes sure to add only your own requests to show in my profile
     * stores user image in prefs
     */
    override fun loadRequests(requestList: ArrayList<OfferRides>, isProfileFrag: Int) {
        Timber.v("RequestList: %s", requestList)
        var isUserImageSaved = false
        findDrivesList = ArrayList()
        if (requestList.isEmpty()) {
            view?.showToast("There are no drive requests...")
        } else {
            val storedUserId = Prefs(mContext).getUserId()
            for (request in requestList) {
                val requestTemp = replaceCharsForDisplay(request)
                if (isProfileFrag == 0) {
                    view?.addNewRequestsRV(requestTemp)
                } else {
                    if (requestTemp.userId.toInt() == storedUserId) {
                        if(!isUserImageSaved){
                            Prefs(mContext).setUserImage(requestTemp.image as String)
                            isUserImageSaved = true
                        }
                        view?.addNewRequestsRV(requestTemp)
                    }
                }
                findDrivesList.add(requestTemp)
            }
        }
    }

    /**
     * Drive filtering based on the cities
     */
    override fun filterDrives(query: String): ArrayList<OfferRides> {
        val requests = getDrivesList()
        val queryToLower = query.toLowerCase()

        val filteredRequestList = ArrayList<OfferRides>()
        for (request in requests) {
            val text = request.fromName.toLowerCase() + " to " + request.toName.toLowerCase()
            if (text.contains(queryToLower)) {
                filteredRequestList.add(request)
            }
        }
        return filteredRequestList
    }

    private fun replaceCharsForDisplay(request: OfferRides): OfferRides {
        val fromName = request.fromName.replace('%', '\\').replace('*', '\'')
        val fromAddress = request.fromAddress.replace('%', '\\').replace('*', '\'')
        val toName = request.toName.replace('%', '\\').replace('*', '\'')
        val toAddress = request.toAddress.replace('%', '\\').replace('*', '\'')
        val polyline = request.polyline.replace('%', '\\').replace('*', '\'')
        request.fromName = fromName
        request.fromAddress = fromAddress
        request.toName = toName
        request.toAddress = toAddress
        request.polyline = polyline
        return request
    }

    /**
     * Stores the drives list for recycler view filtering
     */
    override fun getDrivesList(): List<OfferRides> {
        return findDrivesList.reversed()
    }

    override fun onDestroy() {
        view = null
        if (call?.isDisposed == false) call?.dispose()
    }
}
