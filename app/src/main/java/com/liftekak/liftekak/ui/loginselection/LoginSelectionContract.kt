package com.liftekak.liftekak.ui.loginselection

import android.os.Bundle
import com.facebook.AccessToken
import com.google.firebase.auth.FirebaseUser

/**
 * Created by Sachinda on 2/2/2018.
 *
 */
interface LoginSelectionContract {
    interface View {
        fun goToRegister()
        fun goToUserDashboard()
        fun showToast(message: String)
        fun fbRegisterCallback()
//        fun changeFbButtonText()
    }

    interface Presenter {
        fun validateUserByEmail(email: String, password: String)
        fun validateUserByFacebook(token: AccessToken, fbBundle: Bundle)
        fun checkForUserAccountOnDB() : Boolean
        fun writeFbUserToDB(fbBundle: Bundle)
        fun onDestroy()
    }
}