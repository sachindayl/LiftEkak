package com.liftekak.liftekak.ui.loginemail

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.gson.GsonBuilder
import com.liftekak.liftekak.models.dbModels.Users
import com.liftekak.liftekak.models.dbModels.ResponseModel
import com.liftekak.liftekak.services.UsersAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber


/**
 * Created by Sachinda on 2/3/2018.
 * User Registration
 */
class LoginEmailPresenterImpl(var view: LoginEmailContract.View, var context: Context, val mAuth: FirebaseAuth, val mDatabase: DatabaseReference, val dbUrl: String) : LoginEmailContract.Presenter {
    override fun isPasswordMatching(password: String, confirmation: String): Boolean {
        return password == confirmation
    }

    override fun createUser(newUser: Users) {
        try {
            mAuth.createUserWithEmailAndPassword(newUser.email, newUser.password as String)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Timber.d("createUserWithEmail:success")
                            view.showToast("Hello ${newUser.firstName}!")
                            registerUserOnDB(newUser)
                            view.gotoUserDashboard(mAuth.currentUser!!)
                        } else {
                            // If sign in fails, display a message to the user.
                            Timber.v(task.exception, "createUserWithEmail:failure")
                            view.showToast("Authentication failed. ${task.exception?.message.toString()}")
                        }
                    }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Creates a user on the database
     */
    override fun registerUserOnDB(newUser: Users) {
        try {
            Timber.v("Writing new user")
            val user = mAuth.currentUser
            if (user != null) {
                Timber.v("User uid: %s", user.uid)
//                Single.fromCallable {
                //                    val userId = mAuth.currentUser?.uid
//                    val childUpdates = HashMap<String, Any?>()
//                    childUpdates["/${DBConstants.users}/$userId/first_name"] = newUser.first_name
//                    childUpdates["/${DBConstants.users}/$userId/last_name"] = newUser.last_name
//                    childUpdates["/${DBConstants.users}/$userId/email"] = newUser.email
//                    childUpdates["/${DBConstants.users}/$userId/phone_number"] = newUser.phone_number
//                    childUpdates["/${DBConstants.users}/$userId/is_admin"] = false
//                    childUpdates["/${DBConstants.users}/$userId/created_at"] = ServerValue.TIMESTAMP
//
//                    mDatabase.updateChildren(childUpdates)

                val gSon = GsonBuilder()
                        .setLenient()
                        .create()
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
                val retrofit = Retrofit.Builder()
                        .baseUrl(dbUrl)
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create(gSon))
                        .build()

                val registerAPI = retrofit.create(UsersAPI::class.java)
                val call = registerAPI.insertCustomer(newUser.firstName, newUser.lastName, newUser.email, newUser.phoneNumber as String, user.uid)
                call.enqueue(object : Callback<ResponseModel> {
                    override fun onResponse(call: Call<ResponseModel>?, response: Response<ResponseModel>?) {
                        val responseModel = response?.body()
                        Timber.v("response check: %s", responseModel)
                    }

                    override fun onFailure(call: Call<ResponseModel>?, t: Throwable?) {
                        Timber.v(t, "response check: %s", call.toString())
                    }
                })
//                }
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .doOnSuccess { view.showToast("Registration Successful!$body") }
//                        .doOnError { view.showToast("Registration Failed") }
//                        .subscribe()

            } else {
                view.showToast("User Not Logged In!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}