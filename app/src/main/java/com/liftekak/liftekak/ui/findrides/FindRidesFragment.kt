package com.liftekak.liftekak.ui.findrides


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.adapters.FindRidesRecyclerAdapter
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.AskForRides
import com.liftekak.liftekak.ui.askforride.AskForRideFragment
import com.liftekak.liftekak.ui.offerride.OfferRideFragment
import com.liftekak.liftekak.ui.vehicleselect.VehicleSelectFragment
import kotlinx.android.synthetic.main.app_bar_tab.*
import kotlinx.android.synthetic.main.fragment_find_rides.*
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 */
class FindRidesFragment : BaseTabFragment(), FindRidesContract.View {

    private lateinit var mPrefs: Prefs
    private lateinit var presenter: FindRidesContract.Presenter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var requestList: ArrayList<AskForRides>
    private lateinit var adapter: FindRidesRecyclerAdapter
    private lateinit var rvFr: RecyclerView
    private var isProfileBundle: Bundle? = null
    private var isProfileFrag: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_find_rides, container, false)
        setHasOptionsMenu(true)
        isProfileBundle = arguments
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct.appbar_tab_layout.visibility = View.VISIBLE
        tabAct.supportActionBar!!.title = "Lift Ekak"
        isProfileFrag = isProfileBundle?.getInt("isProfileFrag")
        mPrefs = Prefs(mContext)
        rvFr = tabAct.findViewById(R.id.rv_fr)
        setupListeners()
        linearLayoutManager = LinearLayoutManager(tabAct)
        rvFr.layoutManager = linearLayoutManager
        requestList = ArrayList()
        adapter = FindRidesRecyclerAdapter(requestList, isProfileFrag as Int, dbUrl)
        rvFr.adapter = adapter
        presenter = FindRidesPresenterImpl(this, mContext, mDatabase, mAuth, dbUrl)
    }

    /**
     * check is it is my profile fragment or not and if not, will
     * show the buttons
     */
    override fun setupListeners() {
        if (isProfileFrag == 0) {
            fab_fr_action_request_ride.setOnClickListener {
                val rideRequestFrag = AskForRideFragment()
                tabAct.supportFragmentManager.beginTransaction()
                        .replace(R.id.main_tab_container, rideRequestFrag)
                        .addToBackStack("Ask For A Ride")
                        .commit()
            }

            fab_fr_action_offer_ride.setOnClickListener {
                if (mPrefs.getOfferRidesVehicle()) {
                    val offerRidesFrag = OfferRideFragment()
                    tabAct.supportFragmentManager.beginTransaction()
                            .replace(R.id.main_tab_container, offerRidesFrag)
                            .addToBackStack("Offer A Ride")
                            .commit()
                } else {
                    val vehicleSelectFrag = VehicleSelectFragment()
                    tabAct.supportFragmentManager.beginTransaction()
                            .replace(R.id.main_tab_container, vehicleSelectFrag)
                            .addToBackStack("Vehicle Select")
                            .commit()
                }
            }
        } else {
            fab_fr_multiple_actions_rides.visibility = View.GONE
        }
        sr_fr.setOnRefreshListener {
            requestList = ArrayList()
            adapter = FindRidesRecyclerAdapter(requestList, isProfileFrag as Int, dbUrl)
            rvFr.adapter = adapter
            showLoadingIndicator()
            presenter.getRides(isProfileFrag as Int)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        val search = menu?.findItem(R.id.action_search)
        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Search Rides..."
        search.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM or MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val filteredRequestList = presenter.filterRides(newText as String)
                adapter.animateTo(filteredRequestList)
                rvFr.scrollToPosition(0)
                return true
            }
        })

        search.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                adapter.animateTo(requestList)
                rvFr.scrollToPosition(0)
                return true
            }
        })
    }

    override fun addNewRequestsRV(request: AskForRides) {
        if (!requestList.contains(request)) {
            requestList.add(0, request)
            rvFr.adapter.notifyItemInserted(requestList.size)
            Timber.v("Adapter Size at addNew rides: %s", adapter.itemCount)
        }
    }

    override fun removeRequestsRV(request: AskForRides) {
        adapter.deleteItem(request)
    }

    override fun showLoadingIndicator() {
        pb_fr?.visibility = View.VISIBLE
    }

    override fun hideLoadingIndicator() {
        pb_fr?.visibility = View.INVISIBLE
    }

    override fun stopRefresh() {
        if (sr_fr != null && sr_fr.isRefreshing) {
            sr_fr.isRefreshing = false
        }
    }

    override fun onStart() {
        super.onStart()
        showLoadingIndicator()
        presenter.getRides(isProfileFrag as Int)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

}
