package com.liftekak.liftekak.ui.vehicleselect


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.BaseTabFragment
import com.liftekak.liftekak.base.Notify
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.ui.offerride.OfferRideFragment
import kotlinx.android.synthetic.main.app_bar_tab.*
import kotlinx.android.synthetic.main.fragment_car_select.*
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 * Saves Vehicle
 */
class VehicleSelectFragment : BaseTabFragment(), VehicleSelectContract.View {

    private lateinit var presenter: VehicleSelectContract.Presenter
    private lateinit var mPrefs: Prefs
    private lateinit var passengersAdapter: ArrayAdapter<String>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_car_select, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct.appbar_tab_layout.visibility = View.VISIBLE
        tabAct.supportActionBar!!.title = "Enter Vehicle"
        mPrefs = Prefs(mContext)
        presenter = VehicleSelectPresenterImpl(mContext, mDatabase, mAuth)
        setSpinner()
        showRide()
        btn_set_vehicle.setOnClickListener{
            setRide()
            mPrefs.setOfferRidesVehicle(true)
            Notify(mContext).showToast("Vehicle Successfully Saved!")
            goToOfferRides()
        }

    }


    override fun setRide() {
        val make = et_make.text.toString()
        val model = et_model.text.toString()
        val year  = et_year.text.toString()
        val color = et_vehicle_color.text.toString()
        val plate = et_licence.text.toString()
        val passengers = spn_number_passengers.selectedItem.toString()
        Timber.v("passengers selected: %s", spn_number_passengers.selectedItem.toString())
        mPrefs.setRide(make,model,year,color,plate, passengers)
        presenter.writeVehicleToDB(make, model, year, color, plate,passengers)
    }

    override fun showRide() {
        val carList = mPrefs.getRide()
        Timber.v("carList: %s", carList[5])
        et_make.setText(carList[0])
        et_model.setText(carList[1])
        et_year.setText(carList[2])
        et_vehicle_color.setText(carList[3])
        et_licence.setText(carList[4])

        if(carList[5] == "" || carList[5] == "Maximum Number of Passengers"){
            spn_number_passengers.setSelection(0)
        }else{
            val passengerPos = carList[5].toInt()
            spn_number_passengers.setSelection(passengerPos)
        }
    }

    override fun goToOfferRides() {
        val offerRidesFrag = OfferRideFragment()
        tabAct.supportFragmentManager.beginTransaction()
                .replace(R.id.main_tab_container, offerRidesFrag)
                .addToBackStack("Offer A Ride")
                .commit()
    }

    override fun setSpinner() {
        val numPassList = ArrayList<String>()
        numPassList.add("Maximum Number of Passengers")
        numPassList.add("1")
        numPassList.add("2")
        numPassList.add("3")
        numPassList.add("4")
        numPassList.add("5")
        passengersAdapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_dropdown_item, numPassList)
        spn_number_passengers.adapter = passengersAdapter
    }
}
