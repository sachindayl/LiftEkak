package com.liftekak.liftekak.ui.vehicleselect

/**
 * Created by Sachinda on 2/11/2018.
 */
interface VehicleSelectContract {
    interface View {
        fun setRide()
        fun setSpinner()
        fun showRide()
        fun goToOfferRides()
    }

    interface Presenter {
        fun writeVehicleToDB(make: String, model: String, year: String, color: String, plate: String, passengers: String)
    }
}