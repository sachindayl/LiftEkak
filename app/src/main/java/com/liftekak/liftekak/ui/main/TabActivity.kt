package com.liftekak.liftekak.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.facebook.AccessToken
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.LiftEkakApplication
import com.liftekak.liftekak.base.Notify
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.ui.myprofile.MyProfileFragment
import com.liftekak.liftekak.ui.tabhost.TabHostFragment
import timber.log.Timber

class TabActivity : AppCompatActivity() {
    private lateinit var presenter: TabActivityContract.Presenter
    lateinit var mAuth: FirebaseAuth
    lateinit var mDatabase: DatabaseReference
    lateinit var mContext: Context
    lateinit var dbUrl: String
    private var exitWarningGiven: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab)
        val toolbar = findViewById<Toolbar?>(R.id.toolbar_tab)
        setSupportActionBar(toolbar)
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance().reference
        mContext = LiftEkakApplication.applicationContext()
        dbUrl = resources.getString(R.string.database_url)
        presenter = TabActivityPresenterImpl(mDatabase, mAuth, mContext, dbUrl)

        if(!presenter.isUserIdAvailable()) presenter.getUserId()
        supportFragmentManager.beginTransaction()
                .replace(R.id.main_tab_container, TabHostFragment())
                .addToBackStack("Tab Host")
                .commit()
    }

    override fun onBackPressed() {
        val fragCount = supportFragmentManager.backStackEntryCount
//        Timber.v("FragmentCount: %s", fragCount)
        if( fragCount > 1){
            exitWarningGiven = false
        }
        if(fragCount == 1){
            if(exitWarningGiven){
//                System.exit(0)
                super.onBackPressed()
                super.onBackPressed()
            }else{
                Notify(mContext).showToast("Press BACK again to exit")
                exitWarningGiven = true
            }
        }else{
            super.onBackPressed()
        }


    }

    override fun onStart() {
        super.onStart()
        presenter.pullFuelPrice()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            android.R.id.home ->{
                onBackPressed()
            }
            R.id.my_profile ->{
                supportFragmentManager.beginTransaction()
                        .replace(R.id.main_tab_container, MyProfileFragment())
                        .addToBackStack("My Profile")
                        .commit()
            }

            R.id.action_settings -> return true

            R.id.sign_out -> {
                FirebaseAuth.getInstance().signOut()
                Prefs(mContext).setUserId(0)
                if (AccessToken.getCurrentAccessToken() != null) {
                    AccessToken.setCurrentAccessToken(null)
                }
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

}
