package com.liftekak.liftekak.ui.main

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.gson.GsonBuilder
import com.liftekak.liftekak.services.RetrofitService
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.models.dbModels.FuelPrice
import com.liftekak.liftekak.services.UsersAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

/**
 * Created by Sachinda on 4/6/2018.
 */
class TabActivityPresenterImpl(val mDatabase: DatabaseReference, val mAuth: FirebaseAuth, val mContext: Context, private val dbUrl: String) : TabActivityContract.Presenter {
    override fun pullFuelPrice() {
        val fuelAPIService = RetrofitService().fuelAPIService(dbUrl)
        fuelAPIService.getPetrolDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setFuelPrice)

    }

    private fun setFuelPrice(fuelList: ArrayList<FuelPrice>) {
//        Timber.v("Petrol Value: %s", fuelList[0])
        Prefs(mContext).setPetrolFuelPrice(fuelList[0])
    }

    /**
     * Checks if user id is already stored in the prefs
     */
    override fun isUserIdAvailable(): Boolean {
        var idAvailable = false
        val id = Prefs(mContext).getUserId()
        Timber.v("UserId already available: %s", id)
        if (id > 0) {
            idAvailable = true
        }
        return idAvailable
    }

    /**
     * Getting user id from the database
     */
    override fun getUserId() {
        val userService = usersService()
        userService.getUserId(mAuth.currentUser?.uid as String)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::storeUserId, this::handleError)
    }

    /**
     * Storing the user id in the prefs
     */
    private fun storeUserId(userId: Int) {
        Timber.v("Storing UserId: %s", userId)
        Prefs(mContext).setUserId(userId)
    }

    private fun handleError(error: Throwable) {
        Timber.v(error, "Could not find user id: %s", error.localizedMessage)
    }

    /**
     * Observable based retrofit service for drives
     */
    private fun usersService(): UsersAPI {

        val gSon = GsonBuilder()
                .setLenient()
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(dbUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gSon))
                .build()

        return retrofit.create(UsersAPI::class.java)
    }
}