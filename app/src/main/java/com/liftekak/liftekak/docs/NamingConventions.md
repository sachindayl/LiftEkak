XML Convention

android:id="@id+/widgetType_classAbbreviation_widgetName"

Widget Types\
tv = Text View\
et = Edit Text\
spn = Spinner\
sb = Snack Bar\
frag = Fragment\
iv = Image View\
sr = Swipe Refresh Layout\
ll = Linear Layout

Class Convention

rr = Ride Request\
or = Offer Request\
le = Login Email\
ls = Login Selection\
fp = Find Passengers\
fr = Find Rides

find rides gets from offer rides\
find passengers gets from ask for a ride
