package com.liftekak.liftekak.interfaces

/**
 * Created by Sachinda on 2/4/2018.
 */
interface Regex{
    fun isNameValid(name: CharSequence): Boolean
    fun isEmailValid(email: CharSequence): Boolean
    fun isPhoneNumberValid(phone: CharSequence): Boolean
    fun isPasswordValid(password: CharSequence): Boolean
}