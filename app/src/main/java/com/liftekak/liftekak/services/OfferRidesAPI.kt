package com.liftekak.liftekak.services

import com.liftekak.liftekak.models.dbModels.OfferRides
import com.liftekak.liftekak.models.dbModels.OfferRidesRequests
import com.liftekak.liftekak.models.dbModels.ResponseModel
import io.reactivex.Observable
import retrofit2.http.*


/**
 * Created by Sachinda on 2/15/2018.
 */
interface OfferRidesAPI {

    @FormUrlEncoded
    @POST("drives/insertDrives.php")
    fun insertDrive(
            @Field("from_name") fromName: String,
            @Field("from_address") fromAddress: String,
            @Field("from_latitude") fromLatitude: Double,
            @Field("from_longitude") fromLongitude: Double,
            @Field("to_name") toName: String,
            @Field("to_address") toAddress: String,
            @Field("to_latitude") toLatitude: Double,
            @Field("to_longitude") toLongitude: Double,
            @Field("phone_number") phoneNumber: String,
            @Field("date") date: String,
            @Field("time") time: String,
            @Field("distance") distance: String,
            @Field("duration") duration: String,
            @Field("recurrence") recurrence: String,
            @Field("passenger_type") passengerType: String,
            @Field("number_of_passengers") numberOfPassengers: Int,
            @Field("polyline") polyline: String,
            @Field("user_id") userUid: String
    ): Observable<ResponseModel>

    @GET("drives/getDrives.php")
    fun getDrives(@Query("user_id") userId: String): Observable<ArrayList<OfferRides>>

    @GET("drives/deleteDrive.php")
    fun deleteDrive(@Query("request_id") requestId: Int): Observable<ResponseModel>

    @FormUrlEncoded
    @POST("drives/insertDriveRequests.php")
    fun insertDriveRequest(
            @Field("offer_ride_id") driveRequestId: Int,
            @Field("user_id") requestUserId: Int
    ): Observable<ResponseModel>

    @GET("drives/getDriveRequests.php")
    fun getDriveRequestDetails(
            @Query("user_id") userId: Int,
            @Query("request_id") requestId: Int
    ): Observable<OfferRidesRequests>
}