package com.liftekak.liftekak.services

import com.liftekak.liftekak.models.directions.Directions
import com.liftekak.liftekak.models.distancematrix.DistanceMatrix
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*


/**
 * Created by Sachinda on 2/15/2018.
 * Google Distance Matrix API
 */
interface MapsAPI {
    companion object {
        //        const val BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/"
        const val BASE_URL = "https://maps.googleapis.com"
    }

    @GET("/maps/api/distancematrix/json")
    fun getDistance(
            @Query("units") units: String,
            @Query("origins") origins: String,
            @Query("destinations") destinations: String,
            @Query("key") key: String
    ): Call<DistanceMatrix>

    @GET("/maps/api/directions/json")
    fun getDirections(
            @Query("units") units: String,
            @Query("origin") origins: String,
            @Query("destination") destinations: String,
            @Query("alternatives") alternatives: Boolean,
            @Query("key") key: String
    ): Observable<Directions>

}