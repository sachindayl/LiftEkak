package com.liftekak.liftekak.services

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Sachinda on 5/5/2018.
 */
class RetrofitService {

    private fun retrofitSetup(dbUrl: String): Retrofit {
        val gSon = GsonBuilder()
                .setLenient()
                .create()

        return Retrofit.Builder()
                .baseUrl(dbUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gSon))
                .build()
    }

    fun retrofitSetupWithInterceptor(dbUrl: String): Retrofit {
        val gSon = GsonBuilder()
                .setLenient()
                .create()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
                .baseUrl(dbUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gSon))
                .build()
    }

    fun mapService(): MapsAPI {
        return retrofitSetup(MapsAPI.BASE_URL).create(MapsAPI::class.java)
    }

    /**
     * Observable based retrofit service for drives
     */
    fun usersAPIService(dbUrl: String): UsersAPI {
        return retrofitSetup(dbUrl).create(UsersAPI::class.java)
    }

    fun offerRidesAPIService(dbUrl: String): OfferRidesAPI {
        return retrofitSetupWithInterceptor(dbUrl).create(OfferRidesAPI::class.java)
    }

    fun askForRidesAPIService(dbUrl: String): AskForRidesAPI {
        return retrofitSetup(dbUrl).create(AskForRidesAPI::class.java)
    }

    fun fuelAPIService(dbUrl: String): FuelAPI {
        return retrofitSetup(dbUrl).create(FuelAPI::class.java)
    }

    fun imagesAPIService(dbUrl: String): ImagesAPI {
        return retrofitSetup(dbUrl).create(ImagesAPI::class.java)
    }
}