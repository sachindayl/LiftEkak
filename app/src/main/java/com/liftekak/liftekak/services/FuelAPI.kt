package com.liftekak.liftekak.services

import com.liftekak.liftekak.models.dbModels.FuelPrice
import io.reactivex.Observable
import retrofit2.http.*


/**
 * Created by Sachinda on 2/15/2018.
 * Google Distance Matrix API
 */
interface FuelAPI {

    @GET("pricing/getFuelInfo.php")
    fun getPetrolDetails(): Observable<ArrayList<FuelPrice>>

}