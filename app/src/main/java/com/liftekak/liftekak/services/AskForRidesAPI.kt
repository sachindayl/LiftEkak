package com.liftekak.liftekak.services

import com.liftekak.liftekak.models.dbModels.AskForRides
import com.liftekak.liftekak.models.dbModels.ResponseModel
import io.reactivex.Observable
import retrofit2.http.*


/**
 * Created by Sachinda on 2/15/2018.
 */
interface AskForRidesAPI {

    @FormUrlEncoded
    @POST("rides/insertRides.php")
    fun insertRide(
            @Field("from_name") fromName: String,
            @Field("from_address") fromAddress: String,
            @Field("from_latitude") fromLatitude: Double,
            @Field("from_longitude") fromLongitude: Double,
            @Field("to_name") toName: String,
            @Field("to_address") toAddress: String,
            @Field("to_latitude") toLatitude: Double,
            @Field("to_longitude") toLongitude: Double,
            @Field("phone_number") phoneNumber: String,
            @Field("date") date: String,
            @Field("time") time: String,
            @Field("recurrence") recurrence: String,
            @Field("polyline") polyline: String,
            @Field("user_id") userUid: String
    ): Observable<ResponseModel>

    @GET("rides/getRides.php")
    fun getRides(@Query("user_id") userId: String): Observable<ArrayList<AskForRides>>

    @GET("rides/deleteRide.php")
    fun deleteRide(@Query("request_id") requestId: Int): Observable<ResponseModel>

}