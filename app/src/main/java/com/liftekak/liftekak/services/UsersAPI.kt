package com.liftekak.liftekak.services

import com.liftekak.liftekak.models.dbModels.Users
import com.liftekak.liftekak.models.dbModels.ResponseModel
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by Sachinda on 2/15/2018.
 * Google Distance Matrix API
 */
interface UsersAPI {

    @FormUrlEncoded
    @POST("users/insertCustomer.php")
    fun insertCustomer(
            @Field("first_name") firstName: String,
            @Field("last_name") lastName: String,
            @Field("email") email: String,
            @Field("phone_number") phoneNumber: String,
            @Field("uid") userUid: String
    ): Call<ResponseModel>

    @GET("users/getUserId.php")
    fun getUserId(
            @Query("user_id") userId: String
    ): Observable<Int>

    @GET("users/getCustomerName.php")
    fun getCustomerName(
            @Query("user_id") userId: Int
    ): Observable<Users>


}