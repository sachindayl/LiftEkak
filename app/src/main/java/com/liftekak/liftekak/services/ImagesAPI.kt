package com.liftekak.liftekak.services

import com.liftekak.liftekak.models.dbModels.ResponseModel
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface ImagesAPI {

    @Multipart
    @POST("images/imageUpload.php")
    fun uploadImage(@Part file: MultipartBody.Part, @Query("user_id") userId: String): Observable<ResponseModel>

}