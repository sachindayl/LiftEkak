package com.liftekak.liftekak.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.liftekak.liftekak.R
import com.liftekak.liftekak.ui.main.MainActivity
import kotlinx.android.synthetic.main.app_bar_main.*

/**
 * Created by Sachinda on 2/3/2018.
 */
open class BaseFragment:Fragment(){
    lateinit var mainAct: MainActivity
    lateinit var mContext: Context
    lateinit var mAuth: FirebaseAuth
    lateinit var mDatabase: DatabaseReference
    lateinit var databaseUrl: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainAct = view.context as MainActivity
        mContext = LiftEkakApplication.applicationContext()
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance().reference
        databaseUrl = resources.getString(R.string.database_url)
    }

    fun showToast(message:String){
        Notify(mainAct).showToast(message)
    }

    fun showSnackBar(view: View, message:String){
        Notify(mainAct).showSnackBar(view ,message)
    }
}