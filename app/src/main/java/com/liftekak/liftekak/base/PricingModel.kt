package com.liftekak.liftekak.base

import android.content.Context
import com.liftekak.liftekak.base.constants.Prefs
import timber.log.Timber

/**
 * Created by Sachinda on 4/6/2018.
 */
class PricingModel {
    fun calculateCost(distance: String, passengers: String?, mContext: Context): String {
        val distanceValue = distance.substringBefore(" ")
        var passengersCopy = passengers
        if (passengers == "null" || passengers == null) passengersCopy = "0"
        val distanceToLong = distanceValue.toFloat()
        val passengersToInt = passengersCopy!!.toInt()
        val prefs = Prefs(mContext)
        val petrolDetails = prefs.getPetrolFuelPrice()
        val costPerKmCity = petrolDetails.fuelPrice.toFloat() / petrolDetails.cityMileage.toFloat()
        val costPerKmOutStation = petrolDetails.fuelPrice.toFloat() / petrolDetails.outStationMileage.toFloat()
        val amount: Float
        amount = if (distanceToLong <= 30.0) {
            (distanceToLong * costPerKmCity) / (passengersToInt + 1)
        } else {
            (distanceToLong * costPerKmOutStation) / (passengersToInt + 1)
        }
        val amountToInt = amount.toInt().plus(1)
        //TODO add service charge?
        return amountToInt.toString()
    }
}