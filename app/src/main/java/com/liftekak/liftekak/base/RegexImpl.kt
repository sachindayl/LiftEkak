package com.liftekak.liftekak.base

import com.liftekak.liftekak.interfaces.Regex
import java.util.regex.Pattern

/**
 * Created by Sachinda on 2/4/2018.
 */
class RegexImpl: Regex{
    override fun isNameValid(name: CharSequence): Boolean {
        val namePattern = Pattern.compile("^[^±!@£\$%^&*_+§¡€#¢¶•ªº«\\\\/<>?:;|=.,]{1,20}\$")
        return namePattern.matcher(name).matches()
    }

    override fun isEmailValid(email: CharSequence): Boolean {
        val emailRegex = "(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        val emailPattern = Pattern.compile(emailRegex)
        return emailPattern.matcher(email).matches()
    }

    override fun isPhoneNumberValid(phone: CharSequence): Boolean {
        val phonePattern = Pattern.compile("^\\+?([0-9])+\$")
        return phonePattern.matcher(phone).matches()
    }

    override fun isPasswordValid(password: CharSequence): Boolean {
        val passPattern = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\W]).{6,20})")
        return passPattern.matcher(password).matches()
    }
}