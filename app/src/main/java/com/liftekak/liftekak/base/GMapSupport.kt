package com.liftekak.liftekak.base

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import java.util.*


/**
 * Created by Sachinda on 3/20/2018.
 */
class GMapSupport {


    /**
     * Decodes an encoded path string into a sequence of LatLngs.
     */
    fun decodePolyLines(encodedPaths: ArrayList<String>): ArrayList<ArrayList<LatLng>> {
        val pathList = ArrayList<ArrayList<LatLng>>()
        for(i in 0 until encodedPaths.size){
            val len = encodedPaths[i].length
            // For speed we preallocate to an upper bound on the final length, then
            // truncate the array before returning.
            val path = ArrayList<LatLng>()
            var index = 0
            var lat = 0
            var lng = 0

            while (index < len) {
                var result = 1
                var shift = 0
                var b: Int
                do {
                    b = encodedPaths[i][index++].toInt() - 63 - 1
                    result += b shl shift
                    shift += 5
                } while (b >= 0x1f)
                lat += if (result and 1 != 0) (result shr 1).inv() else result shr 1

                result = 1
                shift = 0
                do {
                    b = encodedPaths[i][index++].toInt() - 63 - 1
                    result += b shl shift
                    shift += 5
                } while (b >= 0x1f)
                lng += if (result and 1 != 0) (result shr 1).inv() else result shr 1

                path.add(LatLng(lat * 1e-5, lng * 1e-5))
            }
            pathList.add(path)
        }

        return pathList
    }

    fun createDirectionBounds(decodedPolyline: ArrayList<ArrayList<LatLng>>): ArrayList<LatLngBounds> {
        val builder = LatLngBounds.Builder()
        val boundsList = ArrayList<LatLngBounds>()
        for(i in 0 until decodedPolyline.size){
            for (ctr in 0 until decodedPolyline[i].size - 1) {
                builder.include(decodedPolyline[i][ctr])
            }
            boundsList.add(builder.build())
        }
        return boundsList
    }

    fun decodePolyLine(encodedPaths: ArrayList<String>): ArrayList<ArrayList<LatLng>>{

        val pathList = ArrayList<ArrayList<LatLng>>()
        for(i in 0 until encodedPaths.size){
            val len = encodedPaths[i].length
            var index = 0
            val decoded = ArrayList<LatLng>()
            var lat = 0
            var lng = 0

            while (index < len) {
                var b: Int
                var shift = 0
                var result = 0
                do {
                    b = encodedPaths[i][index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dLat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lat += dLat

                shift = 0
                result = 0
                do {
                    b = encodedPaths[i][index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dLng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lng += dLng

                decoded.add(LatLng(
                        lat / 100000.0,
                        lng / 100000.0
                ))
            }
            pathList.add(decoded)
        }

        return pathList
    }


}