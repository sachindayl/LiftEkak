package com.liftekak.liftekak.base.constants

import com.google.android.gms.maps.model.LatLng

/**
 * Created by Sachinda on 12/27/2017.
 * Constants for user maps
 */
class MapConstants{
    companion object {
        const val PERMISSIONS_REQUEST_ACCESS_LOCATION = 101
        const val PERMISSIONS_REQUEST_CALL_PHONE = 102
        const val PERMISSIONS_REQUEST_READ_EXTERNAL_FILES = 103
        const val DEFAULT_ZOOM : Float = 15F
        const val M_MAX_ENTRIES = 5
        val M_DEFAULT_LOCATION = LatLng(-33.8523341, 151.2106085)
        var TWENTY_M_RADIUS_NOTIFY_COUNT: Int = 0
        var FIVE_M_RADIUS_NOTIFY_COUNT: Int = 0
        val SUCCESS_RESULT = 0
        val FAILURE_RESULT = 1
        val PACKAGE_NAME = "com.liftekak.liftekak"
        val RECEIVER = PACKAGE_NAME + ".RECEIVER"
        val RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY"
        val LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA"

    }
}