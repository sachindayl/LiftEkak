package com.liftekak.liftekak.base

import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast


/**
 * Created by Sachinda on 12/12/2017.
 * Notifying Methods
 */
class Notify(val context: Context) {

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showSnackBar(view: View, message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    }

    fun getBuilder(): AlertDialog.Builder {
//        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            AlertDialog.Builder(context, R.style.Theme_Material_Dialog_Alert)
//        } else {
//            AlertDialog.Builder(context)
//        }
        return AlertDialog.Builder(context)
    }
}