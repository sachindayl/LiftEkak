package com.liftekak.liftekak.base.adapters

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.LiftEkakApplication
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.base.inflate
import com.liftekak.liftekak.models.dbModels.OfferRides
import com.liftekak.liftekak.ui.drivedetails.DriveDetailsFragment
import com.liftekak.liftekak.ui.main.TabActivity
import kotlinx.android.synthetic.main.rv_item_request.view.*
import timber.log.Timber

/**
 * Created by Sachinda on 2/18/2018.
 * Recycler View that creates the Find Rides Portal
 */
class FindDrivesRecyclerAdapter(private val _requests: ArrayList<OfferRides>, _isProfileFrag: Int, val dbUrl: String) : RecyclerView.Adapter<FindDrivesRecyclerAdapter.ViewHolder>() {
    private val isProfileFrag = _isProfileFrag
    //    private val requests = ArrayList(_requests)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = parent.inflate(R.layout.rv_item_request, false)
        return ViewHolder(inflatedView, _requests, isProfileFrag, dbUrl)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(_requests[position])
    }

    fun deleteItem(cancelledItem: OfferRides) {
        for (request in _requests) {
            if (request.requestId == cancelledItem.requestId) {
                val index = _requests.indexOf(request)
                _requests.remove(request)
                notifyItemRemoved(index)
            }
        }
    }


    override fun getItemCount() = _requests.size

    //the class is holding the list view
    class ViewHolder(itemView: View, requestsList: ArrayList<OfferRides>, isProfileFrag: Int, val dbUrl: String) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val isProfFrag = isProfileFrag
        private var view: View = itemView
        private val driveRequests = requestsList
        private var tabAct = view.context as TabActivity

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {

            val rideBundle = Bundle()
            val request = driveRequests[adapterPosition]
            val requestDetails = ArrayList<String>()
            requestDetails.add(request.requestId.toString())
            requestDetails.add(request.fromName)
            requestDetails.add(request.fromAddress)
            requestDetails.add(request.fromLatitude.toString())
            requestDetails.add(request.fromLongitude.toString())
            requestDetails.add(request.toName)
            requestDetails.add(request.toAddress)
            requestDetails.add(request.toLatitude.toString())
            requestDetails.add(request.toLongitude.toString())
            requestDetails.add(request.phoneNumber)
            requestDetails.add(request.date)
            requestDetails.add(request.time)
            requestDetails.add(request.distance)
            requestDetails.add(request.duration)
            requestDetails.add(request.recurrence)
            requestDetails.add(request.passengerType)
            requestDetails.add(request.numberOfPassengers.toString())
            requestDetails.add(request.polyline)
            requestDetails.add(request.userId)
            requestDetails.add(isProfFrag.toString())
            requestDetails.add(request.createdAt)
            if (request.image != null) {
                requestDetails.add(request.image as String)
            } else {
                request.image = ""
                requestDetails.add(request.image as String)
            }


            rideBundle.putStringArrayList("driveDetails", requestDetails)
            val driveDetailsFrag = DriveDetailsFragment()
            driveDetailsFrag.arguments = rideBundle

            tabAct.supportFragmentManager.beginTransaction()
                    .replace(R.id.main_tab_container, driveDetailsFrag)
                    .addToBackStack("DriveDetailsFragment")
                    .commit()
        }

        fun bindItems(ride: OfferRides) {
            val originDestination = "${ride.fromName} to ${ride.toName}"
            val userId = Prefs(LiftEkakApplication.applicationContext()).getUserId()
            itemView.tv_itemOriginDestination.text = originDestination
            itemView.tv_itemTime.text = ride.time
            itemView.tv_itemDate.text = ride.date
            itemView.tv_itemRecurrence.text = ride.recurrence
            if (ride.recurrence != "Once") {
                itemView.tv_itemDate.visibility = View.INVISIBLE
            }
            itemView.tv_passenger_type.text = ride.passengerType
            if (ride.image == "" || ride.image == null) {
                itemView.iv_itemImage.setImageResource(R.mipmap.ic_launcher)
            } else {
                Timber.v("Drive Image path: %s", ride.image)
                Glide.with(tabAct)
                        .load("${dbUrl}images/$userId/${ride.image}")
                        .apply(RequestOptions.circleCropTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(itemView.iv_itemImage)
            }

        }
    }

    /**
     * Search Filter Logic
     */
    fun animateTo(requestModel: ArrayList<OfferRides>) {
        applyAndAnimateRemovals(requestModel)
        applyAndAnimateAdditions(requestModel)
        applyAndAnimateMovedItems(requestModel)

    }

    private fun applyAndAnimateRemovals(newRequests: ArrayList<OfferRides>) {

        for (i in _requests.size - 1 downTo 0) {
            val request = _requests[i]
            if (!newRequests.contains(request)) {
                removeItem(i)
            }
        }
    }

    private fun applyAndAnimateAdditions(newRequests: ArrayList<OfferRides>) {

        var i = 0
        val count = newRequests.size
        while (i < count) {
            val request = newRequests[i]
            if (!_requests.contains(request)) {
                addItem(i, request)
            }
            i++
        }
    }

    private fun applyAndAnimateMovedItems(newRequests: ArrayList<OfferRides>) {

        var toPosition = newRequests.size - 1
        while (toPosition >= 0) {
            val request = newRequests[toPosition]
            val fromPosition = _requests.indexOf(request)
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition)
            }
            toPosition--
        }
    }

    private fun removeItem(position: Int): OfferRides {
        val model = _requests.removeAt(position)
        notifyItemRemoved(position)
        return model
    }

    private fun addItem(position: Int, model: OfferRides) {
        _requests.add(position, model)
        notifyItemInserted(position)
    }

    private fun moveItem(fromPosition: Int, toPosition: Int) {
        val model = _requests.removeAt(fromPosition)
        _requests.add(toPosition, model)
        notifyItemMoved(fromPosition, toPosition)
    }
}
