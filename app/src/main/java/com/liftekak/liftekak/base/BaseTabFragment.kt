package com.liftekak.liftekak.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.liftekak.liftekak.R
import com.liftekak.liftekak.ui.main.TabActivity

/**
 * Created by Sachinda on 2/3/2018.
 *
 */
open class BaseTabFragment :Fragment(){
    lateinit var tabAct: TabActivity
    lateinit var mContext: Context
    lateinit var mAuth: FirebaseAuth
    lateinit var mDatabase: DatabaseReference
    lateinit var dbUrl: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabAct = view.context as TabActivity
        mContext = LiftEkakApplication.applicationContext()
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance().reference
        dbUrl = resources.getString(R.string.database_url)
    }

    fun showToast(message:String){
        Notify(tabAct).showToast(message)
    }

    fun showSnackBar(view: View, message:String){
        Notify(tabAct).showSnackBar(view ,message)
    }
}