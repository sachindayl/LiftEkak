package com.liftekak.liftekak.base.constants

/**
 * Created by Sachinda on 12/27/2017.
 * Constants for user maps
 */
class DBConstants {

    companion object {
        const val service_requests: String = "service_requests"
        const val fcm_tokens : String = "fcm_tokens"
        const val makes : String = "makes"
        const val models : String = "models"
        const val user_vehicles : String = "user_vehicles"
        const val users : String = "users"
        const val offer_rides: String = "offer_rides"
        const val ask_for_rides: String = "ask_for_rides"
        const val fuel_price: String = "fuel_price"
        const val driver_location: String = "driver_location"
        const val pending_requests : String = "pending_requests"
        const val accepted_requests : String = "accepted_requests"


        const val request_status : String = "requestStatus"


        const val request_pending_acceptance : String = "Request Pending Acceptance"
        const val request_cancelled : String = "Request Cancelled"
        const val request_rejected : String = "Request Rejected"
        const val request_accepted : String = "Request Accepted"

        const val on_route : String = "On Route"
        const val arrived_at_location : String = "Arrived at location"






    }
}