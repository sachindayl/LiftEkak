package com.liftekak.liftekak.base.constants

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.liftekak.liftekak.models.dbModels.FuelPrice

/**
 *
 * Shared Preferences constants
 */
class Prefs(context: Context) {
    val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)

    companion object {
        const val USER_ID = "user_id"
        const val USER_IMAGE = "user_image"
        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: Int = 101
        const val CAR_ADDED = "car_added"
        const val CAR_MAKE = "car_make"
        const val CAR_MODEL = "car_model"
        const val CAR_YEAR = "car_year"
        const val CAR_COLOR = "car_color"
        const val CAR_LICENCE = "car_licence"
        const val CAR_PASSENGERS = "car_passengers"
        const val PHONE_NUMBER_ADDED = "phone_number_added"
        const val PHONE_NUMBER = "phone_number"
        const val OFFER_RIDES_VEHICLE = "offer_rides_first_visit"

        const val PETROL_PRICE = "petrol_price"
        const val DIESEL_PRICE = "diesel_price"
        const val PETROL_CITY_MILEAGE = "petrol_city_mileage"
        const val PETROL_OUTSTATION_MILEAGE = "petrol_outstation_mileage"
    }

    fun setRide(make: String, model: String, year: String, color: String, licence: String, passengers: String) {
        val editor: SharedPreferences.Editor = sharedPrefs.edit()
        editor.putBoolean(Prefs.CAR_ADDED, true)
        editor.putString(Prefs.CAR_MAKE, make)
        editor.putString(Prefs.CAR_MODEL, model)
        editor.putString(Prefs.CAR_YEAR, year)
        editor.putString(Prefs.CAR_COLOR, color)
        editor.putString(Prefs.CAR_LICENCE, licence)
        editor.putString(Prefs.CAR_PASSENGERS, passengers)
        editor.apply()
    }

    fun getRide() : Array<String>{
        val make = sharedPrefs.getString(Prefs.CAR_MAKE, "")
        val model = sharedPrefs.getString(Prefs.CAR_MODEL, "")
        val year = sharedPrefs.getString(Prefs.CAR_YEAR, "")
        val color = sharedPrefs.getString(Prefs.CAR_COLOR, "")
        val plate = sharedPrefs.getString(Prefs.CAR_LICENCE, "")
        val passengers = sharedPrefs.getString(Prefs.CAR_PASSENGERS, "")
        return arrayOf(make,model,year,color,plate,passengers)
    }

    fun setPhone(number: String) {
        val editor: SharedPreferences.Editor = sharedPrefs.edit()
        editor.putBoolean(Prefs.PHONE_NUMBER_ADDED, true)
        editor.putString(Prefs.PHONE_NUMBER, number)
        editor.apply()
    }

    fun getPhone() : String{
        return sharedPrefs.getString(Prefs.PHONE_NUMBER, "")
    }

    fun setOfferRidesVehicle(isSet:Boolean){
        val editor: SharedPreferences.Editor = sharedPrefs.edit()
        editor.putBoolean(Prefs.OFFER_RIDES_VEHICLE, isSet)
        editor.apply()
    }

    fun getOfferRidesVehicle(): Boolean{
        return sharedPrefs.getBoolean(Prefs.OFFER_RIDES_VEHICLE, false)
    }

    fun setPetrolFuelPrice(price: FuelPrice) {
        val editor: SharedPreferences.Editor = sharedPrefs.edit()
        editor.putString(Prefs.PETROL_PRICE, price.fuelPrice)
        editor.putString(Prefs.PETROL_CITY_MILEAGE, price.cityMileage)
        editor.putString(Prefs.PETROL_OUTSTATION_MILEAGE, price.outStationMileage)
        editor.apply()
    }

    fun getPetrolFuelPrice() : FuelPrice {
        val price = FuelPrice()
        price.fuelPrice = sharedPrefs.getString(Prefs.PETROL_PRICE, "")
        price.cityMileage = sharedPrefs.getString(Prefs.PETROL_CITY_MILEAGE, "")
        price.outStationMileage = sharedPrefs.getString(Prefs.PETROL_OUTSTATION_MILEAGE, "")
        return price
    }

    fun getUserId() : Int{
        return sharedPrefs.getInt(Prefs.USER_ID, 0)
    }

    fun setUserId(userId:Int){
        val editor: SharedPreferences.Editor = sharedPrefs.edit()
        editor.putInt(Prefs.USER_ID, userId)
        editor.apply()
    }

    fun getUserImage() : String{
        return sharedPrefs.getString(Prefs.USER_IMAGE, "")
    }

    fun setUserImage(userImage:String){
        val editor: SharedPreferences.Editor = sharedPrefs.edit()
        editor.putString(Prefs.USER_IMAGE, userImage)
        editor.apply()
    }

}