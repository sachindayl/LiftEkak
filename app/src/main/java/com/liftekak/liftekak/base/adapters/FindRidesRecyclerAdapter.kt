package com.liftekak.liftekak.base.adapters

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.liftekak.liftekak.R
import com.liftekak.liftekak.base.LiftEkakApplication
import com.liftekak.liftekak.base.constants.Prefs
import com.liftekak.liftekak.base.inflate
import com.liftekak.liftekak.models.dbModels.AskForRides
import com.liftekak.liftekak.ui.main.TabActivity
import com.liftekak.liftekak.ui.ridedetails.RideDetailsFragment
import kotlinx.android.synthetic.main.rv_ride_request.view.*

/**
 * Created by Sachinda on 2/18/2018.
 * Recycler View that creates the Find Rides Portal
 */
class FindRidesRecyclerAdapter(private val _requests: ArrayList<AskForRides>, private val _isProfileFrag: Int, val dbUrl: String) : RecyclerView.Adapter<FindRidesRecyclerAdapter.ViewHolder>() {
    //    private val requests: ArrayList<AskForRides> = ArrayList(_requests)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = parent.inflate(R.layout.rv_ride_request, false)
        return ViewHolder(inflatedView, _requests, _isProfileFrag, dbUrl)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(_requests[position])
    }

    fun deleteItem(cancelledItem: AskForRides) {
        for (request in _requests) {
            if (request.requestId == cancelledItem.requestId) {
                val index = _requests.indexOf(request)
                _requests.remove(request)
                notifyItemRemoved(index)
            }
        }
    }

    override fun getItemCount() = _requests.size

    //the class is holding the list view
    class ViewHolder(itemView: View, requests: ArrayList<AskForRides>, val isProfileFrag: Int, val dbUrl: String) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var view: View = itemView
        private val rideRequests = requests
        private var tabAct = view.context as TabActivity

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {

            val rideBundle = Bundle()
            val request = rideRequests[adapterPosition]
            val requestDetails = ArrayList<String>()
            requestDetails.add(request.requestId.toString())
            requestDetails.add(request.fromName)
            requestDetails.add(request.fromAddress)
            requestDetails.add(request.fromLatitude.toString())
            requestDetails.add(request.fromLongitude.toString())
            requestDetails.add(request.toName)
            requestDetails.add(request.toAddress)
            requestDetails.add(request.toLatitude.toString())
            requestDetails.add(request.toLongitude.toString())
            requestDetails.add(request.date)
            requestDetails.add(request.time)
            requestDetails.add(request.recurrence)
            requestDetails.add(request.polyline)
            requestDetails.add(request.phoneNumber)
            requestDetails.add(request.userId)
            requestDetails.add(isProfileFrag.toString())
            requestDetails.add(request.createdAt)

            rideBundle.putStringArrayList("rideDetails", requestDetails)
            val rideDetailsFrag = RideDetailsFragment()
            rideDetailsFrag.arguments = rideBundle

            tabAct.supportFragmentManager.beginTransaction()
                    .replace(R.id.main_tab_container, rideDetailsFrag)
                    .addToBackStack("RideDetailsFragment")
                    .commit()
        }

        fun bindItems(ride: AskForRides) {
            val originDestination = "${ride.fromName} to ${ride.toName}"
            val userId = Prefs(LiftEkakApplication.applicationContext()).getUserId()
            itemView.tv_item_rr_itemOriginDestination.text = originDestination
            itemView.tv_item_rr_itemTime.text = ride.time
            itemView.tv_item_rr_itemRecurrence.text = ride.recurrence
            if (ride.image == "") {
                itemView.iv_item_rr_itemImage.setImageResource(R.mipmap.ic_launcher)
            } else {
                Glide.with(tabAct)
                        .load("${dbUrl}images/$userId/${ride.image}")
                        .apply(RequestOptions.circleCropTransform())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(itemView.iv_item_rr_itemImage)
            }
        }
    }

    /**
     * Search Filter Logic
     */
    fun animateTo(requestModel: ArrayList<AskForRides>) {
        applyAndAnimateRemovals(requestModel)
        applyAndAnimateAdditions(requestModel)
        applyAndAnimateMovedItems(requestModel)

    }

    private fun applyAndAnimateRemovals(newRequests: ArrayList<AskForRides>) {

        for (i in _requests.size - 1 downTo 0) {
            val request = _requests[i]
            if (!newRequests.contains(request)) {
                removeItem(i)
            }
        }
    }

    private fun applyAndAnimateAdditions(newRequests: ArrayList<AskForRides>) {

        var i = 0
        val count = newRequests.size
        while (i < count) {
            val request = newRequests[i]
            if (!_requests.contains(request)) {
                addItem(i, request)
            }
            i++
        }
    }

    private fun applyAndAnimateMovedItems(newRequests: ArrayList<AskForRides>) {

        for (toPosition in newRequests.indices.reversed()) {
            val request = newRequests[toPosition]
            val fromPosition = _requests.indexOf(request)
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition)
            }
        }
    }

    private fun removeItem(position: Int): AskForRides {
        val model = _requests.removeAt(position)
        notifyItemRemoved(position)
        return model
    }

    private fun addItem(position: Int, model: AskForRides) {
        _requests.add(position, model)
        notifyItemInserted(position)
    }

    private fun moveItem(fromPosition: Int, toPosition: Int) {
        val model = _requests.removeAt(fromPosition)
        _requests.add(toPosition, model)
        notifyItemMoved(fromPosition, toPosition)
    }
}
