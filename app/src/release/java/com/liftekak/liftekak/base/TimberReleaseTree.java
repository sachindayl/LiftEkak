package com.liftekak.liftekak.base;

import android.util.Log;

import timber.log.Timber;

/**
 * Created by Sachinda on 12/9/2017.
 * Application used for production
 */
class TimberReleaseTree extends Timber.Tree {

    private static final int MAX_LOG_LENGTH = 4000;
    private static final String CRASHLYTICS_KEY_PRIORITY = "priority";
    private static final String CRASHLYTICS_KEY_TAG = "tag";
    private static final String CRASHLYTICS_KEY_MESSAGE = "message";

    @Override
    protected boolean isLoggable(String tag, int priority) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
            return false;
        }
        //Only log WARN, ERROR, WTF
        return true;
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable throwable) {
        if (isLoggable(tag, priority)) {

//            if(priority == Log.ERROR && throwable != null){
//                Crashlytics.setInt(CRASHLYTICS_KEY_PRIORITY, priority);
//                Crashlytics.setString(CRASHLYTICS_KEY_TAG, tag);
//                Crashlytics.setString(CRASHLYTICS_KEY_MESSAGE, message);
//            }
//            if (throwable == null) {
//                Crashlytics.logException(new Exception(message));
//            } else {
//                Crashlytics.logException(throwable);
//            }

            //Message is short enough, does not need to be broken in to chunks.
            if (message.length() < MAX_LOG_LENGTH) {
                if (priority == Log.ASSERT) {
                    Log.wtf(tag, message);
                } else {
                    Log.println(priority, tag, message);
                }
                return;
            }

            //Split by line, then ensure each line can fit in to Log's maximum length.
            for (int i = 0, length = message.length(); i < length; i++) {
                int newLine = message.indexOf('\n', i);
                newLine = newLine != -1 ? newLine : length;
                do {
                    int end = Math.min(newLine, i + MAX_LOG_LENGTH);
                    String part = message.substring(i, end);
                    if (priority == Log.ASSERT) {
                        Log.wtf(tag, part);
                    } else {
                        Log.println(priority, tag, part);
                    }
                    i = end;
                } while (i < newLine);
            }
        }
    }
}

