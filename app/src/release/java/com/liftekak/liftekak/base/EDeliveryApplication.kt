package com.liftekak.liftekak.base

import android.app.Application
import android.content.Context
import timber.log.Timber

/**
 * Created by Sachinda on 12/9/2017.
 * Application used for publishing
 */
class LiftEkakApplication : Application() {

    lateinit var mContext: Context

    override fun onCreate() {
        super.onCreate()
        mContext = applicationContext
        Timber.plant(TimberReleaseTree())
    }

}
