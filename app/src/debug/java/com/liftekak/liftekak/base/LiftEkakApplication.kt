package com.liftekak.liftekak.base

import android.app.Application
import android.content.Context
import com.jakewharton.threetenabp.AndroidThreeTen
import com.liftekak.liftekak.models.dbModels.FuelPrice
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber

/**
 * Created by Sachinda on 12/9/2017.
 * Application for debugging
 */

class LiftEkakApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: LiftEkakApplication? = null
        var fuelPrice: FuelPrice? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }

        fun pricingModel(): FuelPrice {
            return fuelPrice!!
        }
    }

    override fun onCreate() {
        super.onCreate()
//        LiftEkakApplication.database = Room.databaseBuilder(this, RoomDB::class.java, "e-delivery").build()
        Timber.plant(Timber.DebugTree())
        if (LeakCanary.isInAnalyzerProcess(this)) return
        LeakCanary.install(this)
        AndroidThreeTen.init(this)
    }


}

